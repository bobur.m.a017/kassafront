import {BaseQueryFn} from "@reduxjs/toolkit/query";
import axios, {AxiosError, AxiosRequestConfig} from "axios";
import {baseUrl as baseUrl2} from "../allFunc/AllFuncs";
import {toast} from "react-toastify";

export const saveToken = (data: any) => {
    localStorage.setItem("Authorization", "Bearer " + data?.accessToken);
    localStorage.setItem("role",data?.role);
}
export const getToken = () => {
    return localStorage.getItem("Authorization");
}
export const getRoleStorage = () => {
    return localStorage.getItem("role");
}
export const getRefreshToken = () => {
    return localStorage.getItem("Refresh");
}
export const headersToken = () => {
    return {Authorization: getToken()}
}
export const axiosBaseQuery =
    (
        {baseUrl}: { baseUrl: string } = {baseUrl: ''}
    ): BaseQueryFn<
        {
            url: string
            method: AxiosRequestConfig['method']
            data?: AxiosRequestConfig['data']
            params?: AxiosRequestConfig['params']
            headers?: AxiosRequestConfig['headers']
        },
        unknown,
        unknown
    > =>
        async ({url, method, data, params, headers}) => {
            try {
                const result = await axios({url: baseUrl + url, method, data, params, headers});
                // console.log(params,"params")
                console.log(result.data, "data");
                toast.success(result.data);
                return {data: result.data}
            } catch (axiosError) {
                let err = axiosError as AxiosError<any>
                console.log(err.response?.data?.error_message, "errrrr");
                console.log(err?.response?.status, "err kode");
                    // @ts-ignore
                    toast.error(`${err.response?.data}` || err.message);
                    if (err.response?.data?.error_message?.startsWith("JWT") && err.response.status === 401){
                        localStorage.removeItem("Authorization");
                        window.history.pushState("object or string", "Title", "/");
                        window.location.reload();
                    }
                    return {
                        error: {
                            status: err.response?.status,
                            data: err.response?.data || err.message,
                        },
                    }
            }
        }
