import {configureStore} from "@reduxjs/toolkit";
import {foodsApi} from "../reducers/food/FoodApi";
import loading from "../reducers/loading/Loading"
import {userApi} from "../reducers/user/UserApi";
import {categoryApi} from "../reducers/category/CategoryApi";
import {orderApi} from "../reducers/order/OrderApi";
import {productApi} from "../reducers/product2/ProductsApi2";
import {inoutInoutProductsApi} from "../reducers/inoutProduct/InoutProductsApi";
import {warehouseProductApi} from "../reducers/warehouseProduct/WarehouseProduct";
import {companyApi} from "../reducers/company/CompanyApi";
import {restaurantApi} from "../reducers/restaurant/RestaurantApi";
import {deliveryApi} from "../reducers/delivery/DeliveryApi";
import {workDayApi} from "../reducers/workDay/WorkDayApi";
import {outputProductsApi} from "../reducers/OutputProduct/OutputProductsApi";

export const store = configureStore({
    reducer: {
        [inoutInoutProductsApi.reducerPath]: inoutInoutProductsApi.reducer,
        [foodsApi.reducerPath]: foodsApi.reducer,
        [deliveryApi.reducerPath]: deliveryApi.reducer,
        [outputProductsApi.reducerPath]: outputProductsApi.reducer,
        [workDayApi.reducerPath]: workDayApi.reducer,
        [companyApi.reducerPath]: companyApi.reducer,
        [restaurantApi.reducerPath]: restaurantApi.reducer,
        [warehouseProductApi.reducerPath]: warehouseProductApi.reducer,
        [productApi.reducerPath]: productApi.reducer,
        [orderApi.reducerPath]: orderApi.reducer,
        [categoryApi.reducerPath]: categoryApi.reducer,
        [userApi.reducerPath]: userApi.reducer,
        loading
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware(
        {
            serializableCheck:false
        }
    ).concat([
        inoutInoutProductsApi.middleware,
        warehouseProductApi.middleware,
        workDayApi.middleware,
        outputProductsApi.middleware,
        orderApi.middleware,
        deliveryApi.middleware,
        restaurantApi.middleware,
        companyApi.middleware,
        categoryApi.middleware,
        foodsApi.middleware,
        productApi.middleware,
        userApi.middleware,
    ])
})
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
