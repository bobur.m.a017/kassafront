import {IWarehouseProduct} from "./WarehouseProductDTO";
export interface IOutputProduct {
    productId?: number,
    "id"?: number,
    name?: string,
    weight?: number,
    "price"?: number,
    "totalPrice"?: string,
    "totalWeight"?: number,
    "createdDate"?: number,
    maxWeight?:number|string
    productDTO?: IWarehouseProduct
}