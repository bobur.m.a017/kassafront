export interface IUser {
    id:string,
    accessToken?:string,
    refreshToken:string,
    imageUrl:string
    name:string,
    username:string,
    fatherName:string,
    surname?:string
    state?:boolean
}
