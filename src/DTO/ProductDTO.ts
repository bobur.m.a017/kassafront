import {ICategory} from "./CategoryDTO";
import {IWarehouseProduct} from "./WarehouseProductDTO";

export interface IFood {
    id?: string,
    imgUrl?: string
    name?: string,
    price?: number,
    description?: string,
    count?: number,
    category?: ICategory,
    productCompositionDTOS?: IProductCompositionDTOS[]
}

export interface IProductCompositionDTOS {
    id?: number,
    weight?: string|number,
    price?: string|number,
    totalPrice?: string|number,
    createdDate?: string|number,
    productDTO?: IWarehouseProduct
}

export interface IInoutProduct {
    productId?: number,
    "id"?: number,
    name?: string,
    "weight"?: number,
    "price"?: number,
    "totalPrice"?: string,
    "totalWeight"?: number,
    "createdDate"?: number,
    productDTO?: IWarehouseProduct
}

// export interface IProducts {
//     id?: number,
//     name?: string
// }
