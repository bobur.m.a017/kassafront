import {IFood} from "./ProductDTO";
import {IDelivery} from "./DeliveryDTO";

export interface IOrder {
    id?: number,
    countPersons?: number,
    status?: string,
    type?: string,
    phoneNumber?: string,
    address?: string,
    foodOrderDTOS?: IFoodOrders[],
    updateDate?:number,
    deliveryDTO?:IDelivery
}
export interface IFoodOrders {
    "id"?: number,
    "prices"?: number,
    "price"?: number,
    "count"?: number,
    "foodDTO"?:IFood
}
