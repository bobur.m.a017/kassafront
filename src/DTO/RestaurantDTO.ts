import {IWarehouseProduct} from "./WarehouseProductDTO";
import {IInoutProduct} from "./ProductDTO";
import {IStatusWork} from "./StatusWorkDTO";
import {IDelivery} from "./DeliveryDTO";
import {IOrder} from "./OrderDTO";
import {IUser} from "./UserDTO";

export interface IRestaurant{
    id?:number,
    name?:string
    warehouseOfProducts?:IWarehouseProduct[],
    inoutProducts?:IInoutProduct[],
    statusWork?:IStatusWork
    deliveries?:IDelivery,
    orders?:IOrder[]
    userDTOS?:IUser[]
}
