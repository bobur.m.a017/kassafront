
export interface IWarehouseProduct {
    id?:number,
    name?:string,
    "weight"?: number|string,
    "price"?: number,
    "bodyPrice"?: number,
    "totalPrice"?:number
    maxWeight?:number|string
}
