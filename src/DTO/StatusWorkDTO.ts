import {IRestaurant} from "./RestaurantDTO";

export interface IStatusWork {
    id?:number,
    status?:string
    deadLine?:number,
    restaurant?:IRestaurant
}
