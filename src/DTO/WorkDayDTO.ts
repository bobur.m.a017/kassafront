import {IOutputProduct} from "./OutputProductDTO";
import {IInoutProduct, IProductCompositionDTOS} from "./ProductDTO";
import {IWarehouseProduct} from "./WarehouseProductDTO";

export interface IWorkDay {
    id?:number,
    orderCount?:number
    orderAllPrice?:number
    orderAllBodyPrice?:number
    inoutAllPrice?:number
    outputAllPrice?:number
    createdDate?:number
    outputProductDTOS?:IOutputProduct[]
    inoutProductDTOS?:IInoutProduct[]
    productCompositionSaveDTOS?:IProductCompositionDTOS[]
    dayWareHouseOfProductsDTOS?:IWarehouseProduct[]
}
