import {IWarehouseProduct} from "./WarehouseProductDTO";
export interface DayWareHouseOfProductsDTO {
    "id"?: number,
    name?: string,
    "weight"?: number,
    "price"?: number,
    "totalPrice"?: string,
    "totalWeight"?: number,
    "createdDate"?: number,
    productDTO?: IWarehouseProduct
}