interface ILoginData {
    username?: string | undefined,
    password?: string | undefined
}

export const baseUrl = () => {
    // return "http://192.168.1.55:8080/api/v1"
    // return "http://localhost:8080/api/v1"
    return "http://95.130.227.226:8080/api/v1"
}
export const imageUrl = (num: number) => {
    return baseUrl() + "/attachment/" + num
}
export const returnStatus = (num: number | undefined) => {
    if (num === 1) {
        return "statusM"
    } else if (num === 2) {
        return "statusX"
    } else if (num === 3) {
        return "statusY"
    } else if (num === 4) {

    }
}
export const loginUser = (data: ILoginData): boolean => {
    if (data.password === "admin" && data.username === "admin") {
        localStorage.setItem("Authorization", "secret_key")
        return true;
    } else {
        return false;
    }
}
export const toDateInput = (timestamp: number | string|undefined): string => {
    if (timestamp) {
        let strDate = new Date(timestamp).toLocaleString().substring(0, 10);
        return strDate.split(".").reverse().join("-");
    } else {
        return "";
    }
}
export const toDateView = (timestamp: number | string|undefined): string => {
    if (timestamp) {
      return new Date(timestamp).toLocaleString();
    } else {
        return "";
    }
}
