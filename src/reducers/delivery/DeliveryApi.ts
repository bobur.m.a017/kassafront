import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IDelivery} from "../../DTO/DeliveryDTO";


export const deliveryApi = createApi({
    reducerPath: "delivery/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addDelivery"],
    endpoints: build => ({
        getDelivery: build.query<IDelivery[], any>({
            query: () => ({
                url: "/delivery",
                method: "GET",
                headers: headersToken(),
                // params: {deliveryId: params},
            }),
            providesTags: ["addDelivery"]
        }),

        editeDelivery: build.mutation({
            query: (data) => ({
                url: "/delivery",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addDelivery"]
        }),

        relationsDelivery: build.mutation({
            query: ({orderId , deliveryId}) => ({
                url: "/delivery/relations/" + orderId,
                method: "PUT",
                params:{deliveryId},
                headers: headersToken(),
            }),
            invalidatesTags: ["addDelivery"]
        }),

        deleteDelivery: build.mutation<IDelivery, IDelivery>({
            query: (data) => ({
                url: "/delivery/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addDelivery"]
        }),

        addDelivery: build.mutation({
            query: (data) => ({
                url: "/delivery",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addDelivery"]
        }),


    })
})
export const {
    useGetDeliveryQuery,
    useAddDeliveryMutation,
    useEditeDeliveryMutation,
    useDeleteDeliveryMutation,
    useRelationsDeliveryMutation,
} = deliveryApi
