import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IInoutProduct} from "../../DTO/ProductDTO";

export const inoutInoutProductsApi = createApi({
    reducerPath: "inoutInoutProducts/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addInoutProducts"],
    endpoints: build => ({
        getInoutProducts: build.query<IInoutProduct[], any>({
            query: (params) => ({
                url: "/inout-product",
                headers: headersToken(),
                method: "GET",
                params,
            }),
            providesTags: ["addInoutProducts"]
        }),
        editeInoutProducts: build.mutation<IInoutProduct,any>({
            query: (data) => ({
                url: "/inout-product",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addInoutProducts"]
        }),
        deleteInoutProducts: build.mutation<IInoutProduct, IInoutProduct>({
            query: (data) => ({
                url: "/inout-product/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addInoutProducts"]
        }),
        addInoutProducts: build.mutation<IInoutProduct,IInoutProduct>({
            query: (data) => ({
                url: "/inout-product/"+data?.productDTO?.id,
                method: "POST",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addInoutProducts"]
        }),
        testInoutProducts: build.mutation<any,any>({
            query: () => ({
                url: "/inout-product/everyDay",
                method: "GET",
                headers: headersToken(),
                // params: params
            }),
            // invalidatesTags: ["addInoutProducts"]
        }),

    })
})
export const {
    useGetInoutProductsQuery,
    useAddInoutProductsMutation,
    useEditeInoutProductsMutation,
    useDeleteInoutProductsMutation,
    useTestInoutProductsMutation,
} = inoutInoutProductsApi
