import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IUser} from "../../DTO/UserDTO";

export const userApi = createApi({
    reducerPath: "user/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addUser"],
    endpoints: build => ({
        signInUser: build.mutation({
            query: (data) => ({
                url: "/auth",
                method: "POST",
                data,
                headers: {"Content-Type": 'application/x-www-form-urlencoded'},
            }),
            invalidatesTags: ["addUser"]
        }),
        getUser: build.query({
            query: ({data}) => ({
                url: "",
                method: "GET",
                data,
                headers: headersToken(),
            }),
        }),
        getUserRole: build.query({
            query: () => ({
                url: "/role",
                method: "GET",
                headers: headersToken(),
            }),
        }),
        changeStatus: build.mutation<IUser,any>({
            query: (data) => ({
                url: "/user/state",
                method: "PUT",
                headers: headersToken(),
                data
            }),
        }),

    })
})
export const {
    useSignInUserMutation,
    useGetUserQuery,
    useGetUserRoleQuery,
    useChangeStatusMutation
} = userApi
