import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICategory} from "../../DTO/CategoryDTO";


export const categoryApi = createApi({
    reducerPath: "category/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addCategory"],
    endpoints: build => ({
        getCategory: build.query<ICategory[], any>({
            query: ({params}) => ({
                url: "/category",
                method: "GET",
                headers: headersToken(),
                params: {categoryId: params},
            }),
            providesTags: ["addCategory"]
        }),

        editeCategory: build.mutation({
            query: (data) => ({
                url: "/category",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addCategory"]
        }),

        deleteCategory: build.mutation<ICategory, ICategory>({
            query: (data) => ({
                url: "/category/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addCategory"]
        }),

        addCategory: build.mutation({
            query: (data) => ({
                url: "/category",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addCategory"]
        }),


    })
})
export const {
    useGetCategoryQuery,
    useAddCategoryMutation,
    useEditeCategoryMutation,
    useDeleteCategoryMutation,
} = categoryApi
