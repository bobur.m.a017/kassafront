import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IWarehouseProduct} from "../../DTO/WarehouseProductDTO";


export const warehouseProductApi = createApi({
    reducerPath: "warehouseProduct/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addWarehouseProduct"],
    endpoints: build => ({
        getWarehouseProduct: build.query<IWarehouseProduct[], any>({
            query: () => ({
                url: "/warehouse-product",
                method: "GET",
                headers: headersToken(),
                // params: {warehouseProductId: params},
            }),
            providesTags: ["addWarehouseProduct"]
        }),

        editeWarehouseProduct: build.mutation({
            query: (data) => ({
                url: "/warehouse-product",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addWarehouseProduct"]
        }),

        deleteWarehouseProduct: build.mutation<IWarehouseProduct, IWarehouseProduct>({
            query: (data) => ({
                url: "/warehouse-product/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addWarehouseProduct"]
        }),

        addWarehouseProduct: build.mutation({
            query: (data) => ({
                url: "/warehouse-product",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addWarehouseProduct"]
        }),

    })
})
export const {
    useGetWarehouseProductQuery,
    useAddWarehouseProductMutation,
    useEditeWarehouseProductMutation,
    useDeleteWarehouseProductMutation,
} = warehouseProductApi
