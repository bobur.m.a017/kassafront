import {createSlice, PayloadAction} from "@reduxjs/toolkit";


interface ILoading {
    loading: boolean | undefined,
    send?: boolean
    role?: string
}

const initialState: ILoading = {
    loading: false,
    send: false,
    role: "",
}

const loadingSlice = createSlice({
    name: "loading",
    initialState,
    reducers: {
        loadingFunc(state, payload: PayloadAction<boolean | undefined>) {
            state.loading = payload.payload
        },
        smsIs(state, payload: PayloadAction<boolean | undefined>) {
            state.send = payload.payload
        },
        roleFunc(state, payload: PayloadAction<string | undefined>) {
            state.role = payload.payload
        }
    }
})
export const {loadingFunc, smsIs, roleFunc} = loadingSlice.actions
export default loadingSlice.reducer
