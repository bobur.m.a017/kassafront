import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IFood} from "../../DTO/ProductDTO";

export const foodsApi = createApi({
    reducerPath: "foods/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addFoods"],
    endpoints: build => ({
        getFoods: build.query<IFood[], any>({
            query: (params) => ({
                url: "/food/by-category",
                headers: headersToken(),
                method: "GET",
                params,
            }),
            providesTags: ["addFoods"]
        }),
        editeFoods: build.mutation({
            query: (data) => ({
                url: "/food",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addFoods"]
        }),
        deleteFoods: build.mutation<IFood, IFood>({
            query: (data) => ({
                url: "/food/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addFoods"]
        }),
        addFoods: build.mutation({
            query: (data) => ({
                url: "/food",
                method: "POST",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addFoods"]
        }),
        addFoodsComposition: build.mutation({
            query: ({data,id}) => ({
                url: "/food/compositions/"+id,
                method: "POST",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addFoods"]
        }),
        editeFoodsComposition: build.mutation({
            query: ({data,id}) => ({
                url: "/food/compositions/"+id,
                method: "PUT",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addFoods"]
        }),
        addImages: build.mutation({
            query: ({data, id}) => ({
                url: "/food/add-image/" + id,
                method: "POST",
                data,
                headers: headersToken(),
                // params:params
            }),
            invalidatesTags: ["addFoods"]
        }),
        deleteImages: build.mutation({
            query: ({data, params}) => ({
                url: "/food/deleteAttachment/" + data.id,
                method: "DELETE",
                headers: headersToken(),
                params
            }),
            invalidatesTags: ["addFoods"]
        }),
        deleteProductComp: build.mutation({
            query: (data) => ({
                url: "/food/compositions/" + data.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addFoods"]
        }),

        relationToCategory: build.mutation({
            query: (params) => ({
                url: "/food/relation",
                method: "PUT",
                headers: headersToken(),
                params
            }),
            invalidatesTags: ["addFoods"]
        }),

    })
})
export const {
    useGetFoodsQuery,
    useAddFoodsMutation,
    useEditeFoodsMutation,
    useDeleteFoodsMutation,
    useAddImagesMutation,
    useDeleteImagesMutation,
    useRelationToCategoryMutation,
    useAddFoodsCompositionMutation,
    useDeleteProductCompMutation,
    useEditeFoodsCompositionMutation
} = foodsApi
