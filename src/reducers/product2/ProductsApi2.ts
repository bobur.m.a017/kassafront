import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IWarehouseProduct} from "../../DTO/WarehouseProductDTO";

export const productApi = createApi({
    reducerPath: "product/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addProduct"],
    endpoints: build => ({
        getProduct: build.query<IWarehouseProduct[], any>({
            query: (params) => ({
                url: "/products",
                headers: headersToken(),
                method: "GET",
                params,
            }),
            providesTags: ["addProduct"]
        }),
        editeProduct: build.mutation({
            query: (data) => ({
                url: "/products",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addProduct"]
        }),
        deleteProduct: build.mutation<IWarehouseProduct, IWarehouseProduct>({
            query: (data) => ({
                url: "/products/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addProduct"]
        }),
        addProduct: build.mutation({
            query: (data) => ({
                url: "/products",
                method: "POST",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addProduct"]
        }),
    })
})
export const {
    useGetProductQuery,
    useAddProductMutation,
} = productApi
