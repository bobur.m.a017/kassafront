import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IRestaurant} from "../../DTO/RestaurantDTO";


export const restaurantApi = createApi({
    reducerPath: "restaurant/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addRestaurant"],
    endpoints: build => ({
        getRestaurant: build.query<IRestaurant[], any>({
            query: () => ({
                url: "/restaurant",
                method: "GET",
                headers: headersToken(),
                // params: {restaurantId: params},
            }),
            providesTags: ["addRestaurant"]
        }),
        getRestaurantDirector: build.query<IRestaurant[], any>({
            query: () => ({
                url: "/restaurant/director",
                method: "GET",
                headers: headersToken(),
                // params: {restaurantId: params},
            }),
            providesTags: ["addRestaurant"]
        }),
        getRestaurantAdmin: build.query<IRestaurant[], any>({
            query: () => ({
                url: "/restaurant/admin",
                method: "GET",
                headers: headersToken(),
            }),
            providesTags: ["addRestaurant"]
        }),

        editeRestaurant: build.mutation({
            query: (data) => ({
                url: "/restaurant",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addRestaurant"]
        }),

        deleteRestaurant: build.mutation<IRestaurant, IRestaurant>({
            query: (data) => ({
                url: "/restaurant/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addRestaurant"]
        }),

        addRestaurant: build.mutation({
            query: (data) => ({
                url: "/restaurant",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addRestaurant"]
        }),


    })
})
export const {
    useGetRestaurantQuery,
    useGetRestaurantAdminQuery,
    useGetRestaurantDirectorQuery,
    useAddRestaurantMutation,
    useEditeRestaurantMutation,
    useDeleteRestaurantMutation,
} = restaurantApi
