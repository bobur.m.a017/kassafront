import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICompany} from "../../DTO/CompanyDTO";


export const companyApi = createApi({
    reducerPath: "company/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addCompany"],
    endpoints: build => ({
        getCompany: build.query<ICompany[], any>({
            query: () => ({
                url: "/company",
                method: "GET",
                headers: headersToken(),
                // params: {companyId: params},
            }),
            providesTags: ["addCompany"]
        }),
        getCompanyBy: build.query<ICompany, any>({
            query: () => ({
                url: "/company/by",
                method: "GET",
                headers: headersToken(),
                // params: {companyId: params},
            }),
            providesTags: ["addCompany"]
        }),

        editeCompany: build.mutation({
            query: (data) => ({
                url: "/company",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addCompany"]
        }),

        deleteCompany: build.mutation<ICompany, ICompany>({
            query: (data) => ({
                url: "/company/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addCompany"]
        }),

        addCompany: build.mutation({
            query: (data) => ({
                url: "/company",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addCompany"]
        }),


    })
})
export const {
    useGetCompanyQuery,
    useGetCompanyByQuery,
    useAddCompanyMutation,
    useEditeCompanyMutation,
    useDeleteCompanyMutation,
} = companyApi
