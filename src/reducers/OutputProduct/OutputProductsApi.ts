import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IOutputProduct} from "../../DTO/OutputProductDTO";


export const outputProductsApi = createApi({
    reducerPath: "outputProducts/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addOutputProducts"],
    endpoints: build => ({

        editeOutputProducts: build.mutation<IOutputProduct[],any>({
            query: (data) => ({
                url: "/output-product/"+data?.id,
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOutputProducts"]
        }),
        getOutputProducts: build.query<IOutputProduct[],any>({
            query: (params) => ({
                url: "/output-product",
                method: "GET",
                params,
                headers: headersToken(),
            }),
            providesTags: ["addOutputProducts"]
        }),

        addOutputProducts: build.mutation<IOutputProduct,IOutputProduct>({
            query: (data) => ({
                url: "/output-product",
                method: "POST",
                data,
                headers: headersToken(),
                // params: params
            }),
            invalidatesTags: ["addOutputProducts"]
        }),

    })
})
export const {
    useAddOutputProductsMutation,
    useEditeOutputProductsMutation,
    useGetOutputProductsQuery
} = outputProductsApi
