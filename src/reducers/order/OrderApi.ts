import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IFoodOrders, IOrder} from "../../DTO/OrderDTO";


export const orderApi = createApi({
    reducerPath: "order/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addOrder"],
    endpoints: build => ({
        getOrder: build.query<IOrder[], any>({
            query: (params) => ({
                url: "/order",
                method: "GET",
                headers: headersToken(),
                params,
            }),
            providesTags: ["addOrder"]
        }),

        editeOrder: build.mutation({
            query: (data) => ({
                url: "/order",
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrder"]
        }),

        changeFoodOrderStatus: build.mutation({
            query: (data) => ({
                url: "/order/status/" + data.id,
                method: "PUT",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrder"]
        }),

        deleteOrder: build.mutation<IOrder, IOrder>({
            query: (data) => ({
                url: "/order/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrder"]
        }),

        deleteFoodOrder: build.mutation<IFoodOrders, IFoodOrders>({
            query: (data) => ({
                url: "/order/foodOrder/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            // invalidatesTags: ["addOrder"]
        }),

        addOrder: build.mutation<IOrder, any>({
            query: (data) => ({
                url: "/order",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrder"]
        }),

        addOrderDelivery: build.mutation<IOrder, any>({
            query: (data) => ({
                url: "/order/delivery",
                method: "POST",
                data,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrder"]
        }),


    })
})
export const {
    useGetOrderQuery,
    useAddOrderMutation,
    useEditeOrderMutation,
    useDeleteOrderMutation,
    useChangeFoodOrderStatusMutation,
    useDeleteFoodOrderMutation,
    useAddOrderDeliveryMutation
} = orderApi
