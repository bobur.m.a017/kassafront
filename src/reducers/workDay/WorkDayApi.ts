import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {IWorkDay} from "../../DTO/WorkDayDTO";


export const workDayApi = createApi({
    reducerPath: "workDay/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addWorkDay"],
    endpoints: build => ({
        getWorkDay: build.query<IWorkDay, any>({
            query: (data) => ({
                url: "/work-day",
                method: "GET",
                headers: headersToken(),
                params: data,
            }),
            providesTags: ["addWorkDay"]
        }),
        getWorkDays: build.query<IWorkDay[], any>({
            query: (data) => ({
                url: "/work-day/by-date",
                method: "GET",
                headers: headersToken(),
                params: data,
            }),
            providesTags: ["addWorkDay"]
        }),




    })
})
export const {
    useGetWorkDayQuery,
    useLazyGetWorkDayQuery,
    useLazyGetWorkDaysQuery

} = workDayApi
