import React, {useEffect, useRef, useState} from 'react';
import {Button, Form, Modal, Table} from "react-bootstrap";
import {useGetProductQuery} from "../../reducers/product2/ProductsApi2";
import {useTestInoutProductsMutation
} from "../../reducers/inoutProduct/InoutProductsApi";
import {toDateInput, toDateView} from "../../allFunc/AllFuncs";
import {IWarehouseProduct} from "../../DTO/WarehouseProductDTO";
import MyDropDown from "../../components/MyDropDown";
import {
    useAddOutputProductsMutation,
    useEditeOutputProductsMutation,
    useGetOutputProductsQuery
} from "../../reducers/OutputProduct/OutputProductsApi";
import {IOutputProduct} from "../../DTO/OutputProductDTO";

interface IMyInterface {
    startDate?: string,
    endDate?: string
}

function OutputProducts(): JSX.Element {
    const [params, setParams] = useState<IMyInterface>()

    const {data} = useGetProductQuery("");
    const {data: outputProductApi} = useGetOutputProductsQuery("");
    const [addOutputProduct] = useAddOutputProductsMutation()
    const [editeOutputProduct] = useEditeOutputProductsMutation()
    const [productState, setProductState] = useState<IOutputProduct>()
    const [productShow, setProductShow] = useState<boolean>(false)
    const [productList, setProductList] = useState<IWarehouseProduct[]>()
    const [warehouseSate, setWarehouseSate] = useState<IWarehouseProduct>()
    const [show, setShow] = useState(false);
    const [test] = useTestInoutProductsMutation();
    const handler = (e: MouseEvent) => {
        if (ref.current && !ref.current.contains(e.target as Node)) {
            setProductShow(false);
        } else {
            setProductShow(true);
        }
    }
    useEffect(() => {
        setProductList(data);
    }, [data]);
    useEffect(() => {
        document.addEventListener("click", (e) => handler(e))
    }, []);
    const handleClose = () => setShow(false);
    const handleShow = (data: IOutputProduct | undefined) => {
        if (data) {
            setProductState({...data, productId: data.productDTO?.id, name: data.productDTO?.name});
        } else {
            setProductState(data);
        }
        setShow(true)
    };
    const ref = useRef<HTMLInputElement>(null);

    const handleClickOutside = () => {
        setProductShow(true);
    };

    const submitInoutProduct = (e: React.FormEvent) => {
        e.preventDefault();
        if (productState?.id) {
            editeOutputProduct(productState);
        } else {
            addOutputProduct(productState as IOutputProduct);

        }
        handleClose();
    }
    const totalPrice = (e: React.ChangeEvent<HTMLInputElement | undefined>): number => {
        if (e.target.name === "price"&& productState?.weight) {
            return parseFloat(e.target.value) * (productState?.weight ?? 0);
        } else {
            return parseFloat(e.target.value) * (productState?.price || 1);
        }
    }
    const changeProduct = (e: React.ChangeEvent<HTMLInputElement | undefined>) => {
        setProductState({
            ...productState,
            [e.target.name]: e?.currentTarget?.value,
            productId: e.currentTarget.name === "name" ? undefined : productState?.productId,
            totalPrice: totalPrice(e)?.toFixed(2)
        });
        setProductList(data?.filter((item) => item.name?.toLowerCase().includes(e?.target?.value.toLowerCase())))
    }
    const getProductSelect = (item: IWarehouseProduct) => {
        setWarehouseSate(item);
        setProductState({
            ...productState,
            productId: item?.id,
            name: item.name,
            productDTO: item
        })
    }
    const onChangeDate = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        if (e) {
            setParams({...params, [e.target.name]: new Date(e?.target.value).getTime()})
        }
    }
    return (
        <div>
            <nav className={"h-24 flex justify-center py-3"}>
                <div>
                    <label>Sana (dan)</label>
                    <br/>
                    <input type="date" name={"startDate"} onChange={onChangeDate} className={"myInput2"}
                           value={toDateInput(params?.startDate)}/>
                </div>
                <div className={"mx-4"}>
                    <label>Sana (gacha)</label>
                    <br/>
                    <input type="date" name={"endDate"} value={toDateInput(params?.endDate)}
                           className={"myInput2"}
                           min={toDateInput(params?.startDate)}
                           onChange={onChangeDate}/>
                </div>
            </nav>
            {/*<button onClick={() => test(null)}>check</button>*/}
            <div className={"myCard2 myScroll scrollStyle"}>
                <Table className={"inScrollX"} size={"sm"}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Vaqti</th>
                        <th>Vazni</th>
                        <th>Ombordagi holati</th>
                        <th>Narxi</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        outputProductApi?.map((item, index) =>
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td onClick={() => handleShow(item)}
                                    className={"hover:text-blue-400 myHover"}>{item?.productDTO?.name}</td>
                                <td>{toDateView(item?.createdDate)}</td>
                                <td>{item.weight}</td>
                                <td>{item.totalWeight}</td>
                                <td>{item.price}</td>
                            </tr>
                        )
                    }

                    </tbody>
                </Table>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={submitInoutProduct}>
                    <Modal.Header closeButton>
                        <Modal.Title>Omborga kirim qilish </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <label htmlFor="productName">Mahsulot nomi</label>
                        <div className={"relative"}>
                            <MyDropDown item={productState?.productDTO} list={productList} getItems={getProductSelect}/>
                        </div>
                        <br/>
                        <label htmlFor="weigth">Vazni (kg)</label>
                        <br/>
                        <input type="number" required className={"myInput2"}
                               name={"weight"}
                               onChange={changeProduct}
                               step={"0.001"} onWheel={(e) => e?.currentTarget?.blur()}
                               value={productState?.weight || ''}
                        />
                        <br/>
                        <label htmlFor="weigth">Narxi</label>
                        <br/>
                        <input type="number" name={"price"} required className={"myInput2"}
                               onChange={changeProduct}
                               step={"0.001"} onWheel={(e) => e?.currentTarget?.blur()}
                               value={productState?.price || ''}/>
                        <div>Umumiy narx: {productState?.totalPrice}</div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button size={'sm'} variant="secondary" onClick={handleClose}>
                            Yopish
                        </Button>
                        <Button size={'sm'} variant="primary" type={'submit'}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

export default OutputProducts;