import React from 'react';

interface isloading {
    loading: true | false | undefined
}

function IsLoading({loading}:isloading) {
    if (!loading){
        return  null;
    }

    return (
        <div className={"loading"}>

        </div>
    );
}

export default IsLoading;
