import React, {useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import {useAddProductMutation} from "../../reducers/product2/ProductsApi2";

function ProductsAdd(): JSX.Element {
    const [addProduct] = useAddProductMutation();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const submitProduct = (e:React.FormEvent<HTMLFormElement|undefined>) => {
        e?.preventDefault();
        addProduct({name:e?.currentTarget?.productName?.value})
        handleClose();
    }

    return (
        <div>
            <div>
                <button className={"myBgButton min-w-[160px] "} onClick={handleShow}>Mahsulot qo'shish</button>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={submitProduct}>
                    <Modal.Header closeButton>
                        <Modal.Title>Mahsulot</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Label>Mahsulot nomi</Form.Label>
                        <Form.Control size={'sm'} name={'productName'} type={"text"} required/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary"  size={'sm'} onClick={handleClose}>
                            Yopish
                        </Button>
                        <Button variant="primary" size={'sm'} type={'submit'}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

export default ProductsAdd;