
import Main from "./main/Main";
import {Route, Routes} from "react-router-dom";


const Home = () => {
    return (
        <div className={"px-3"}>
            <Routes>
                <Route path={"/"} element={<Main/>}/>
            </Routes>
        </div>
    );
}
export default Home;
