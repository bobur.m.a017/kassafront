import React, {useEffect} from 'react';
import {IUser} from "../../DTO/UserDTO";
import {BiUserCircle} from "react-icons/bi";
import {Colors} from "../../components/Strings";
import {BsPencilSquare} from "react-icons/bs";
import {AiFillDelete} from "react-icons/ai";
import {FaLock, FaUnlockAlt} from "react-icons/fa";
import {useChangeStatusMutation} from "../../reducers/user/UserApi";

interface users {
    users: IUser[] | undefined,
    refetch:()=>void
}

function Users({users,refetch}: users) {
    const [changeStatus, data] = useChangeStatusMutation();
    useEffect(() => {
        if (data.status === "fulfilled") {
            console.log(data.status, "dadada")
            refetch();
        }
    }, [data.status]);
    return (
        <div>
            <div className={"min-w-[400px]"}>
                <div className={"myListMini flex justify-between p-1 font-bold text-lg"}>
                    <div>Rasmi</div>
                    <div>Login</div>
                    <div>Vazifasi</div>
                </div>
                {
                    users?.map((user, index) =>
                        <div key={index} className={"myListMini flex justify-between p-1 text-sm"}>
                            <BiUserCircle color={Colors.GOVERNESS_COLOR} size={25}/>
                            <div>{user?.username}</div>
                            <div>{user?.name}</div>
                            <div className={"flex"}>
                                <span><BsPencilSquare color={'blue'} className={"mx-2 myHover"} size={20}/></span>
                                <span onClick={() => changeStatus(user)}>{user.state ?
                                    <FaUnlockAlt color={Colors.GOVERNESS_COLOR} className={"mx-2 myHover"} size={20}/> :
                                    <FaLock color={'red'} className={"myHover"} size={20}/>}</span>
                                <span><AiFillDelete color={'red'} className={"myHover"} size={20}/></span>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    );
}

export default Users;