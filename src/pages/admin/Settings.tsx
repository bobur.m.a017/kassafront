import React, {useState} from 'react';
import MyTabs from "../../components/MyTabs";
import Delivery from "../delivery/Delivery";

function Settings() {
    const [tabNums, setTabNums] = useState(0);
    return (
        <div>
            <div className={"pt-3"}>
                <MyTabs lists={[{name:"Xodimlar",num:0},{name:"Yetqazuvchilar",num:1},{name:"Xodimlar",num:3}]} onClickItem={setTabNums}/>
            </div>
            {tabNums === 1 ? <Delivery/>:null}
        </div>
    );
}

export default Settings;