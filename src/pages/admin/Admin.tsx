import React, {useEffect} from 'react';
import {Route, Routes} from "react-router-dom";
import "./adminStyle.scss"
import Categories from "../category/Categories";
import Food from "../food/Food";
import Orders from "../orders/Orders";
import Statistics from "../statistics/Statistics";
import {useGetUserRoleQuery} from "../../reducers/user/UserApi";
import {useAppDispatch} from "../../hooks/Hooks";
import {roleFunc} from "../../reducers/loading/Loading";
import DesktopNavbar from "../../components/DesktopNavbar";
import MobileNavbar from "../../components/MobileNavbar";
import Main from "../main/Main";
import InoutProducts from "../inoutProducts/InoutProduct";
import WarehouseProduct from "../warehouseProduct/WarehouseProduct";
import OrdersDelivery from "../orders/OrdersDelivery";
import OrderDelivery from "../orders/OrdersDelivery";
import Settings from "./Settings";
import Users from "./Users";
import Company from "../company/Company";
import InoutAndOutput from "../inoutProducts/InoutAndOutput";

function Admin(): JSX.Element {
    const {data} = useGetUserRoleQuery("");
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(roleFunc(data))
    }, [data]);

    return (
        <div className={""}>
            <DesktopNavbar/>
            <MobileNavbar/>
            <div className={"md:ml-14 mt-0 mb-14 md:mb-0 md:mt-20"}>
                <Routes>
                    <Route path={"/"} element={<Main/>}/>
                    <Route path={"/categories"} element={<Categories/>}/>
                    <Route path={"/products"} element={<Food/>}/>
                    <Route path={"/orders"} element={<Orders/>}/>
                    <Route path={"/statistic"} element={<Statistics/>}/>
                    <Route path={"/inout-output"} element={<InoutAndOutput/>}/>
                    <Route path={"/warehouse-product"} element={<WarehouseProduct/>}/>
                    <Route path={"/orders-delivery"} element={<OrdersDelivery/>}/>
                    <Route path={"/settings-admin"} element={<Company/>}/>
                    <Route path={"/settings"} element={<Settings/>}/>
                </Routes>
            </div>
        </div>
    );
}

export default Admin;
