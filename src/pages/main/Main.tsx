import React, {useState} from 'react';
import MyTabs from "../../components/MyTabs";
const Main = () => {
    const [currentTabs, setCurrentTabs] = useState(0);
    const onClickItem = (num:number) => {
        setCurrentTabs(num);
    }
    return (
        <div className={"px-4 mt-5 myShadow myBorder myScroll p-3"}>
            <MyTabs lists={[{name:"Приход",num:0},{name:"Расход",num:1}]} onClickItem={onClickItem}/>
        </div>
    );
}
export default Main;
