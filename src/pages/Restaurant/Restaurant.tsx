import React, {useState} from 'react';
import {useGetRestaurantDirectorQuery} from "../../reducers/restaurant/RestaurantApi";
import {AiFillDelete} from "react-icons/ai";
import {BsPencilSquare} from "react-icons/bs";
import {Button, Form, Modal} from "react-bootstrap";
import {IRestaurant} from "../../DTO/RestaurantDTO";
import Users from "../admin/Users";

function Restaurant() {
    const {data: restaurant,refetch} = useGetRestaurantDirectorQuery("")
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [restaurantState, setRestaurantState] = useState<IRestaurant | undefined>();
    const setRestaurantShow = (data: IRestaurant | undefined) => {
        if (restaurantState?.id === data?.id) {
            setRestaurantState(undefined);
        } else {
            setRestaurantState(data);
        }
    }
    const submitRestaurant = (e:React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

    }
    return (
        <div>
            <div className={"myCard2 text-center text-xl"}>
                <span className={"font-bold"}>Restaurantlar ro'yxati</span>
                <div className={"myScroll scrollStyle p-1"}>
                    {
                        restaurant?.map((item, index) =>
                            <div key={index} className={"myListMini"}>
                                <div className={"flex justify-between items-center p-2"}>
                                    <span onClick={() => setRestaurantShow(item)}
                                          className={"myHover"}>{item?.name}</span>
                                    <span className={"flex"}>
                                <span><BsPencilSquare color={'blue'} className={"mx-2 myHover"} size={20}/></span>
                                <span><AiFillDelete color={'red'} className={"myHover"} size={20}/></span>
                                </span>
                                </div>
                                <div className={restaurantState?.id === item?.id ? "myAnimated" : "myAnimated2"}>
                                    <Users users={item?.userDTOS} refetch={refetch}/>
                                </div>
                            </div>
                        )
                    }

                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Form>
                    <Modal.Header closeButton>
                        <Modal.Title>Restoran</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Yopish
                        </Button>
                        <Button variant="primary" onClick={handleClose}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    )
        ;
}

export default Restaurant;