import React, {useEffect, useState} from 'react';
import {useGetCompanyByQuery} from "../../reducers/company/CompanyApi";
import Restaurant from "../Restaurant/Restaurant";
import {ICompany} from "../../DTO/CompanyDTO";

function Company() {
    const {data: company} = useGetCompanyByQuery("")
    const [companyState, setCompanyState] = useState<ICompany | undefined>()

    useEffect(() => {
        setCompanyState(company)
    }, [company]);

    return (
        <div>
            <div className={"myCard2"}>
                <div className={"flex justify-between w-100"}>
                    <div>Kompaniya nomi</div>
                    <div className={"font-bold"}>{companyState?.name} </div>
                </div>
            </div>
            <div>
                <Restaurant/>
            </div>
        </div>
    );
}

export default Company;