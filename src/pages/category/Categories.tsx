import React, {useEffect, useState} from 'react';
import {
    useAddCategoryMutation, useDeleteCategoryMutation,
    useEditeCategoryMutation,
    useGetCategoryQuery
} from "../../reducers/category/CategoryApi";
import {ICategory} from "../../DTO/CategoryDTO";
import {AiFillDelete, AiFillEdit} from "react-icons/ai";
import {IoMdAddCircle} from "react-icons/io";
import {Button, Form, Modal} from "react-bootstrap";
import { useGetUserRoleQuery} from "../../reducers/user/UserApi";
import {Roles} from "../../components/Strings";

function Categories() {
    const [category, setCategory] = useState<ICategory[] | undefined>()
    const [categoryState, setCategoryState] = useState<ICategory | undefined>()
    const {data: categoryDate, error: categoryError, isLoading: loadingApi} = useGetCategoryQuery('');
    const [addCategoryApi, resultApi] = useAddCategoryMutation();
    const [editCategoryApi, resultEditeCategoryApi] = useEditeCategoryMutation();
    const [deleteCategoryApi, resultDeleteCategoryApi] = useDeleteCategoryMutation();
    const [show, setShow] = useState(false);
    const {data:role} = useGetUserRoleQuery("");
    const handleClose = () => setShow(false);
    const handleShow = (data: undefined | ICategory) => {
        setCategoryState(data);
        setShow(true);
    };

    useEffect(() => {
        setCategory(categoryDate);
    }, [categoryDate]);
    const submitData = (e: React.FormEvent<HTMLFormElement> | undefined) => {
        e?.preventDefault();
        if (categoryState?.id) {
            editCategoryApi(categoryState);
        } else {
            addCategoryApi(categoryState);
        }
        handleClose();
    }
    const handlerChange = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        setCategoryState({...categoryState, name: e?.target.value})
    }
    return (
        <div>
            <button className={"myBgButton myBorder flex justify-between"} onClick={() => handleShow(undefined)}>
                <IoMdAddCircle size={25} color={"#E5EDFC"}/> <span className={"flex mx-2"}>Tom turlari</span></button>
            <br/>
            <div className={"myScroll"}>
                {category?.map((item, index) =>
                    <div key={index} className={"w-100 flex justify-center"}>
                        <div className={"myList"}>
                            <div>{item.name}</div>
                            {role === Roles.ADMIN ? <div>
                                <button className={"mx-3"} onClick={() => handleShow(item)}><AiFillEdit
                                    color={"success"}/></button>
                                <button onClick={() => deleteCategoryApi(item)}><AiFillDelete color={"red"}/></button>
                            </div>:null}
                        </div>
                    </div>
                )}
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id={"formControl"} onSubmit={submitData}>
                        <Form.Label>Turini nomi</Form.Label>
                        <input type="text" placeholder="Taom turini kiriting" className={"myInput2"}
                               value={categoryState?.name || ''} onChange={handlerChange} required/>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" type={'submit'} form={'formControl'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Categories;