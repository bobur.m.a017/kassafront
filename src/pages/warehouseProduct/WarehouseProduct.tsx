import React, {useEffect, useState} from 'react';
import {useGetWarehouseProductQuery} from "../../reducers/warehouseProduct/WarehouseProduct";
import {Button, Form, Modal, Table} from "react-bootstrap";
import {useAddOutputProductsMutation} from "../../reducers/OutputProduct/OutputProductsApi";
import {IOutputProduct} from "../../DTO/OutputProductDTO";

function WarehouseProduct() {
    const [show, setShow] = useState(false);
    const [wareHouseState, setWareHouseState] = useState<IOutputProduct|undefined>()
    const {data,refetch} = useGetWarehouseProductQuery("");
    const [outPutProduct,result] = useAddOutputProductsMutation()
    const handleClose = () => {
        setWareHouseState({});
        setShow(false)
    };
    useEffect(() => {
        refetch()
    }, [result]);
    const handleShow = (data: IOutputProduct) => {
        setWareHouseState({...data, maxWeight: data?.weight, weight: undefined});
        setShow(true)
    };
    const submitOutput = (e:React.FormEvent) => {
      e.preventDefault();
        outPutProduct(wareHouseState as IOutputProduct);
        handleClose();
    }

    return (
        <div>
            <div className={"myCard2 myScroll scrollStyle"}>
                <Table className={"inScrollX"} size={"sm"}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Vazni</th>
                        <th>Tan narxi</th>
                        <th>Umumiy qiymati</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        data?.map((item, index) =>
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td onClick={() => handleShow(item as IOutputProduct)} className={"myHover"}>{item?.name}</td>
                                <td>{item?.weight}</td>
                                <td>{item?.bodyPrice}</td>
                                <td>{item?.totalPrice}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </Table>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={submitOutput}>
                    <Modal.Header closeButton>
                        <Modal.Title>{wareHouseState?.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Label>Max miqdor: {wareHouseState?.maxWeight}</Form.Label>
                        <Form.Control type={'number'} name={"weight"} value={wareHouseState?.weight || ''} required
                                      max={wareHouseState?.maxWeight}
                                      onChange={(e) => setWareHouseState({
                                          ...wareHouseState,
                                          weight: parseFloat(e?.target?.value)
                                      })} step={"0.0001"}/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Ortga
                        </Button>
                        <Button variant="primary" type={'submit'}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

export default WarehouseProduct;