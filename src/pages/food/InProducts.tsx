import React, {useEffect, useRef, useState} from 'react';
import {IFood, IProductCompositionDTOS} from "../../DTO/ProductDTO";
import {useGetProductQuery} from "../../reducers/product2/ProductsApi2";
import {Form, Modal} from "react-bootstrap";
import {
    useAddFoodsCompositionMutation,
    useDeleteProductCompMutation,
    useEditeFoodsCompositionMutation
} from "../../reducers/food/FoodApi";
import {AiTwotoneDelete} from "react-icons/ai";
import ProductsAdd from "../products/ProductsAdd";
import {IWarehouseProduct} from "../../DTO/WarehouseProductDTO";
import MyDropDown from "../../components/MyDropDown";

interface IThisProps {
    product?: IFood
}

function InProducts({product}: IThisProps) {
    const {data} = useGetProductQuery("");
    const [deleteProductComp] = useDeleteProductCompMutation();
    const [addProductCompApi] = useAddFoodsCompositionMutation()
    const [editeProductCompApi] = useEditeFoodsCompositionMutation()
    const [productList, setProductList] = useState<IWarehouseProduct[]>()
    const [productShow, setProductShow] = useState<boolean>(false);
    const [show, setShow] = useState(false);
    const [food, setFood] = useState<IFood | undefined>()
    const [productCompState, setProductCompState] = useState<IProductCompositionDTOS | undefined>()
    const handleClose = () => setShow(false);
    const handleShow = (data: IFood | undefined) => {
        setFood(data);
        setShow(true);
        setProductCompState({});
    };

    useEffect(() => {
        setProductList(data);
    }, [data]);

    useEffect(() => {
        setProductCompState({});
        setFood(product);
    }, [product]);

    useEffect(() => {
        document.addEventListener("click", (e) => handler(e))
    }, []);
    const ref = useRef<HTMLInputElement>(null);

    const handler = (e: MouseEvent) => {
        if (ref.current && !ref.current.contains(e.target as Node)) {
            setProductShow(false);
        } else {
            setProductShow(true);
        }
    }
    const clickedFood = (data: IFood | undefined) => {
        handleShow(data);
    }
    const changeProduct = (e: React.ChangeEvent<HTMLInputElement | undefined>) => {
        if (!productCompState?.id) {
            setProductList(data?.filter((item) => item.name?.toLowerCase().includes(e?.target?.value.toLowerCase())))
            setProductCompState({
                ...productCompState,
                productDTO: {...productCompState?.productDTO, name: e.currentTarget.value}
            })
        }
    }
    const submitInProduct = (e: React.FormEvent) => {
        e.preventDefault();
        if (productCompState?.id) {
            editeProductCompApi({data: productCompState, id: product?.id})
        } else {
            addProductCompApi({data: productCompState, id: product?.id});
        }
    }
    const handleClickOutside = () => {
        setProductShow(true);
    };

    const changeWeight = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        setProductCompState({...productCompState, weight: e?.currentTarget?.value})
    }
    const clickedProduct = (data: any) => {
        setProductCompState({...productCompState, productDTO: data as IWarehouseProduct})
    }
    return (
        <div>
            <div
                onClick={() => clickedFood(product)}
                className={"myHover hover:text-blue-300"}>{(product?.name?.length ?? "Nomi") > 20 ? product?.name?.substring(0, 25) : product?.name}</div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title className={"flex justify-between"}> <span
                        className={"text-[25px]"}>{product?.name} taomi mahsulotlari</span> <span className={"text-sm"}><span
                        className={"text-red-600"}>Ro'yxatda topilmasa</span> <ProductsAdd/></span> </Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitInProduct}>
                    <Modal.Body>
                        <div className={"md:flex"}>
                            <div>
                                <label>Mahsulot</label>
                                <br/>
                                <MyDropDown item={productCompState?.productDTO} list={productList} getItems={clickedProduct}/>
                            </div>
                            <div className={"flex"}>
                                <div>
                                    <label htmlFor="weigth">Vazni (kg)</label>
                                    <br/>
                                    <input type="number" required className={"myInput max-w-[100px]"}
                                           name={"weight"}
                                           onChange={changeWeight}
                                           step={"0.0001"} onWheel={(e) => e?.currentTarget?.blur()}
                                           value={productCompState?.weight || ""}
                                    />
                                </div>
                                <div className={"mx-3"}>
                                    <br/>
                                    <button className={"myButtonInfo h-[28px]"}
                                            type={"submit"}>{productCompState?.id ? "O'zgartirish" : "ok"}</button>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div>
                            {food?.productCompositionDTOS?.map((item, index) =>
                                <div key={index} className={"myCard2 flex justify-between"}>
                                    <div className={"myHover hover:text-blue-300"}
                                         onClick={() => setProductCompState(item)}>
                                        {item?.productDTO?.name} <span
                                        className={"text-emerald-700"}>  {item.weight} kg</span>
                                    </div>
                                    <div>
                                        <AiTwotoneDelete size={25} color={'red'} className={"myHover"}
                                                         onClick={() => deleteProductComp(item)}/>
                                    </div>
                                </div>
                            )}
                        </div>
                    </Modal.Body>
                </Form>
            </Modal>
        </div>
    );
}

export default InProducts;