import React, {useEffect, useState} from 'react';
import {IoMdAddCircle} from "react-icons/io";
import {Button, Form, Modal} from "react-bootstrap";
import {ICategory} from "../../DTO/CategoryDTO";
import {IFood} from "../../DTO/ProductDTO";
import {
    useAddImagesMutation,
    useAddFoodsMutation, useDeleteFoodsMutation, useEditeFoodsMutation,
    useGetFoodsQuery,
    useRelationToCategoryMutation
} from "../../reducers/food/FoodApi";
import {AiFillDelete, AiFillEdit} from "react-icons/ai";
import { useGetCategoryQuery} from "../../reducers/category/CategoryApi";
import imgs from "../../images/img.png"
import {Roles} from "../../components/Strings";
import {useGetUserRoleQuery} from "../../reducers/user/UserApi";
import InProducts from "./InProducts";

function Product() {
    let params = {categoryId: null};
    const [show, setShow] = useState(false);
    const [numberRender, setNumberRender] = useState(0)
    const [productList, setProductList] = useState<IFood[] | undefined>()
    const [productState, setProductState] = useState<IFood | undefined>()
    const [categoryState, setCategoryState] = useState<ICategory | undefined>()
    const {data: productDate, refetch: getFoods} = useGetFoodsQuery({categoryId: categoryState?.id});
    const [addProductApi] = useAddFoodsMutation();
    const {data: categoryData, error: categoryError, isLoading: loadingApi} = useGetCategoryQuery("");
    const [relationApi] = useRelationToCategoryMutation();
    const [addImageApi] = useAddImagesMutation();
    const {data: role} = useGetUserRoleQuery("");
    const [editeProductApi] = useEditeFoodsMutation();
    const [deleteProduct] = useDeleteFoodsMutation();
    const handleClose = () => setShow(false);
    const handleShow = (data: undefined | IFood, num: number) => {
        setProductState(data);
        setNumberRender(num);
        setShow(true);
    };
    useEffect(() => {

        setProductList([]);
        setProductList(productDate)
    }, [productDate]);
    const submitData = (e: React.FormEvent<HTMLFormElement> | undefined) => {
        e?.preventDefault();
        if (productState?.id) {
            editeProductApi(productState);
        } else {
            addProductApi(productState);
        }
        handleClose();
    }
    const submitImage = (e: React.FormEvent<HTMLFormElement> | undefined) => {
        e?.preventDefault();
        let formDate = new FormData();
        formDate.append("file", e?.currentTarget?.myImage?.files[0]);
        addImageApi({data: formDate, id: productState?.id});
        handleClose()
    }
    const handlerChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setProductState({...productState, [e.target.name]: e?.target?.value})
    }
    const selectCategory = (data: ICategory | undefined) => {
        setCategoryState(data);
        if (categoryState) {
            setProductList([]);
            // window.location.reload();
            getFoods();
        }
    }
    const relationFoodToCategory = (e: React.ChangeEvent<HTMLSelectElement>, data: IFood) => {
        relationApi({categoryId: e.target.value, foodId: data?.id})
    }
    const renderAddFood = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id={"formControl"} onSubmit={submitData}>
                        <Form.Label>Taom nomi</Form.Label>
                        <input type="text" placeholder="Taom nomini kiriting" className={"myInput2"}
                               value={productState?.name || ''} name={"name"} onChange={handlerChange} required/>
                        <br/>
                        <Form.Label>Taom narxi</Form.Label>
                        <input type="number" placeholder="Taom narxini kiriting" className={"myInput2"}
                               value={productState?.price || ''} name={"price"} onChange={handlerChange} required/>
                        <br/>
                        <Form.Label>Taom haqida</Form.Label>
                        <textarea name={"description"} onChange={handlerChange} value={productState?.description || ''}
                                  className={"myInput2"}/>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" type={'submit'} form={'formControl'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </>
        )
    }
    const renderAddImage = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>Rasm qo'shish</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id={"formControl"} onSubmit={submitImage}>
                        <Form.Label>Rasmni tanlang</Form.Label>
                        <input type="file" accept="image/*" className={"myInput2"}
                               name={"myImage"} required/>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" type={'submit'} form={'formControl'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </>
        )
    }

    return (
        <div className={" px-2"}>
            <button className={"myBgButton myBorder flex justify-between"} onClick={() => handleShow(undefined, 0)}>
                <IoMdAddCircle size={25} color={"#E5EDFC"}/> <span className={"flex mx-2"}>Tom qo'shish</span></button>
            <br/>
            <div className={"w-100 md:flex h-[88vh]"}>
                <div className={"myScroll md:w-[40%]"}>
                    <div className={""}>
                        <div className={"flex justify-center px-2"}>
                            <div className={"myList"} onClick={() => selectCategory(undefined)}>
                                <div>Hammasi</div>
                                <div>
                                    <input type="radio" checked={!categoryState?.id} readOnly/>
                                </div>
                            </div>
                        </div>
                        {categoryData?.map((item, index) =>
                            <div key={index} className={"flex justify-center px-2"}>
                                <div className={"myList"} onClick={() => selectCategory(item)}>
                                    <div>{item.name}</div>
                                    <div>
                                        <input type="radio" checked={item.id === categoryState?.id} readOnly/>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className={"w-[100%] h-100 md:w-[70%] lg:w-[70%] flex justify-center h-max max-h-[89.5vh]"}>
                    <div
                        className={"grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 myScroll scrollStyle w-100 place-items-center"}>
                        {
                            productList?.map((item, index) =>
                                <div key={index} className={"myCard mx-2"}>
                                    <div className={"w-100 h-[100px] imgCard"} onClick={() => handleShow(item, 1)}
                                         style={{backgroundImage: `url(${item.imgUrl ? item.imgUrl : imgs})`}}>
                                    </div>
                                    <div className={"min-h-[50px]"}>
                                        <div className={"text-sm"}>
                                            <InProducts product={item}/>
                                        </div>
                                        <div className={"font-bold"}>
                                            {item.price} so'm
                                        </div>
                                    </div>
                                    {role === Roles.ADMIN ? <div className={"w-100 flex justify-between"}>
                                        <button className={"mx-3"} onClick={() => handleShow(item, 0)}><AiFillEdit
                                            color={"success"}/></button>
                                        <select name="categoryId" defaultValue={item.category?.id || ""}
                                                onChange={(e) => relationFoodToCategory(e, item)}>
                                            <option value="">Taom turi</option>
                                            {
                                                categoryData?.map((category, index1) =>
                                                    <option value={category.id}
                                                            key={index1}>{category?.name?.substring(0, 10)}</option>
                                                )
                                            }
                                        </select>
                                        <button onClick={() => deleteProduct(item)}><AiFillDelete color={"red"}/>
                                        </button>
                                    </div> : null}
                                </div>
                            )
                        }
                    </div>
                </div>

            </div>
            <Modal show={show} onHide={handleClose}>
                {
                    numberRender === 0 ? renderAddFood() : null
                }
                {
                    numberRender === 1 ? renderAddImage() : null
                }
            </Modal>
        </div>
    );
}

export default Product;
