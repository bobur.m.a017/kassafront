import React, {useEffect, useState} from 'react';
import './style.scss'
import {saveToken} from "../../store/axiosApi";
import {useNavigate} from "react-router-dom";
import {useGetUserQuery, useSignInUserMutation} from "../../reducers/user/UserApi";
import {AiFillLock, AiOutlineUser} from "react-icons/ai";

function LoginPage() {
    const history = useNavigate();
    const [data2, setData] = useState({username: '', password: ''});
    const [loginUser, userLogin] = useSignInUserMutation()
    // const {data, isLoading, error, refetch} = useGetUserQuery("")
    // useEffect(() => {
    //     console.log(data)
    // }, [data]);
    useEffect(() => {
        if (userLogin.data) {
            saveToken(userLogin.data);
            if (userLogin?.data?.user_role === "") {
                history("/admin");
            } else {
                history("/");
            }
        }
    }, [userLogin]);

    const loginSubmit = (e: React.FormEvent<HTMLFormElement> | undefined) => {
        e?.preventDefault();
        const querystring = require('qs');
        const formDate = new FormData();
        formDate.append("username",e?.currentTarget?.username.value)
        formDate.append("password",e?.currentTarget?.password.value)
        querystring.stringify({username: e?.currentTarget?.username.value, password: e?.currentTarget?.password.value})
        console.log(querystring);
        loginUser({username: e?.currentTarget?.username.value, password: e?.currentTarget?.password.value});
        // refetch();
    }
    return (
        <div className="container d-flex justify-content-center items-center" style={{height: '100vh'}}>
            <div className="wrapper w-full max-w-[400px]">
                <div className="title"><span>Tizimga kirish</span></div>
                <form onSubmit={loginSubmit}>
                    <div className="row">
                        <i className="fas">
                            <AiOutlineUser/>
                        </i>
                        <input type="text" placeholder=" Logini kiriting" name={"username"} required/>
                    </div>
                    <div className="row">
                        <i className={'fas'}> <AiFillLock/></i>
                        <input type="password" placeholder="Parolni kiriting" name={"password"} required/>
                    </div>

                    <div className="row button">
                        <button className={"buttonLogin"} type={'submit'}>KIRISH</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default LoginPage;
