export interface ILoginUser {
    access_token?:string,
    refresh_token?:string,
    user_role?:string
}
export interface IStateMessage {
    text?:string
    success?:boolean
    code?:number

}
export interface ILoginToUse {
    phoneNumber?:string
    code?:number

}
