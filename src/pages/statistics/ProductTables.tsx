import React, {useRef} from 'react';
import {Button, Table} from "react-bootstrap";
import {toDateView} from "../../allFunc/AllFuncs";
import {useDownloadExcel} from "react-export-table-to-excel";

interface IPropses {
    list?: any[],
    fileName?:string
}

function ProductTables({list,fileName}: IPropses) {

    const tableRef = useRef(null);

    const {onDownload} = useDownloadExcel({
        currentTableRef: tableRef.current,
        filename: "excel-"+fileName,
        sheet: "excel-"+fileName
    })

    return (
        <div className={"border m-2 myScroll scrollStyle"}>
            <Button variant={"outline-success"} onClick={onDownload} size={"sm"}>Excelga yuklash</Button>
            <div className={"max-h-[300px]"}>
                <Table size={'sm'}  ref={tableRef} border={1}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Vazni</th>
                        <th>Narxi</th>
                        <th>Umumiy narxi</th>
                        <th>Vaqti</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        list?.map((item, index) =>
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{item?.productDTO ? item?.productDTO?.name : item?.name}</td>
                                <td>{item?.weight}</td>
                                <td>{item?.price}</td>
                                <td>{item?.totalPrice}</td>
                                <td>{toDateView(item?.createdDate)}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </Table>

            </div>
        </div>
    );
}

export default ProductTables;