import React, {useEffect, useState} from 'react';
import {toDateInput} from "../../allFunc/AllFuncs";
import {useLazyGetWorkDayQuery} from "../../reducers/workDay/WorkDayApi";
import {Splide, SplideSlide} from "@splidejs/react-splide";
import '@splidejs/react-splide/css/core';
import '@splidejs/react-splide/css';
import MyTabs from "../../components/MyTabs";
import ProductTables from "./ProductTables";
import OneWorkDay from "./OneWorkDay";

interface IParams {
    startDate?: number,
    endDate?: number
}

function DateByDate() {
    const [tabNum, setTabNum] = useState(0)
    const [params, setParams] = useState<IParams>();
    const [getWorkDays, {data}] = useLazyGetWorkDayQuery()
    useEffect(() => {
        console.log(data, "data")
    }, [data]);
    const onChangeDate = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        if (e) {
            setParams({...params, [e.target.name]: new Date(e?.target.value).getTime()});
            if (params?.endDate || e.target.name === "endDate") {
                getWorkDays({...params, [e.target.name]: new Date(e?.target.value).getTime()});
            }
        }
    }
    return (
        <div className={"max-w-[700px] bg-white w-100"}>
            <nav className={"h-24 flex justify-center py-3"}>
                <div>
                    <label>Sana (dan)</label>
                    <br/>
                    <input type="date" name={"startDate"} onChange={onChangeDate} className={"myInput2"}
                           value={toDateInput(params?.startDate)}/>
                </div>
                <div className={"mx-4"}>
                    <label>Sana (gacha)</label>
                    <br/>
                    <input type="date" name={"endDate"} value={toDateInput(params?.endDate)}
                           min={toDateInput(params?.startDate)}
                           className={"myInput2"}
                           disabled={!params?.startDate}
                           onChange={onChangeDate}/>
                </div>
            </nav>
            <div>

            </div>
            <OneWorkDay data={data}/>
        </div>
    );
}

export default DateByDate;