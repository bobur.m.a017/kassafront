import React, {useEffect, useRef, useState} from 'react';
import {toDateInput} from "../../allFunc/AllFuncs";
import {Button, Modal, OverlayTrigger, Table, Tooltip} from "react-bootstrap";
import {useGetOrderQuery} from "../../reducers/order/OrderApi";
import {useDownloadExcel} from "react-export-table-to-excel";
import {Colors, OrderType} from "../../components/Strings";
import {IFoodOrders, IOrder} from "../../DTO/OrderDTO";
import {useGetDeliveryQuery} from "../../reducers/delivery/DeliveryApi";
import MyDropDown from "../../components/MyDropDown";
import {IDelivery} from "../../DTO/DeliveryDTO";
import {MdClear} from "react-icons/md";
import {IFood} from "../../DTO/ProductDTO";
import {useGetFoodsQuery} from "../../reducers/food/FoodApi";

interface IMyInterface {
    startDate?: string,
    endDate?: string,
    orderType?: string | null
}

function OrderStatistics() {
    const param = {startDate: '', endDate: '', orderType: ""};
    const [deliveryState, setDeliveryState] = useState<IDelivery>()
    const [params, setParams] = useState<IMyInterface>()
    const {data} = useGetOrderQuery(params)
    const {data: deliveries} = useGetDeliveryQuery("")
    const {data: foods} = useGetFoodsQuery("")
    const [foodState, setFoodState] = useState<IFood>()
    const [orderList, setOrderList] = useState<IOrder[]>()
    const [foodList, setFoodList] = useState<IFoodOrders[]>()
    const [show, setShow] = useState(false);
    const [ordersInfo, setOrdersInfo] = useState<IOrder>()


    useEffect(() => {
        setOrderList(data);
    }, [data]);
    const handleClose = () => setShow(false);
    const handleShow = () => {
        setFoodState({});
        setFoodList([]);
        setShow(true);
    }

    useEffect(() => {

    }, [orderList]);


    const onChangeDate = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        if (e) {
            setParams({...params, [e.target.name]: new Date(e?.target.value).getTime()})
        }
    }
    const tableRef = useRef(null);

    const {onDownload} = useDownloadExcel({
        currentTableRef: tableRef.current,
        filename: 'Buyurtmalar',
        sheet: 'Buyurtma'
    })
    const colorSt = (text: string | undefined): string => {
        if (text === "To'lanmagan") {
            return Colors.NO_PAID
        } else {
            return Colors.PAID
        }
    }
    const getDelivery = (dataDelivery: IDelivery | undefined) => {
        if (dataDelivery) {
            setOrderList(data?.filter((item, index) => item?.deliveryDTO?.id === dataDelivery.id));
        } else {
            setOrderList(data);
        }
        setDeliveryState(dataDelivery);
    }
    const getOrderType = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setParams({...params, orderType: e.currentTarget?.value === "" ? null : e.currentTarget?.value})
    }
    const getFoods = (food: IFood) => {
        let foodOrders: IFoodOrders[] = [];
        data?.forEach((order, index) =>
            order?.foodOrderDTOS?.forEach((foodOrderItem) => {
                    if (foodOrderItem?.foodDTO?.name === food?.name) {
                        foodOrders.push(foodOrderItem)
                    }
                }
            )
        )
        setFoodState(food);
        setFoodList(foodOrders);
    }
    return (
        <div className={"max-w-[700px] bg-white w-100"}>
            <nav className={"h-24 flex justify-center py-3"}>
                <div>
                    <label>Sana (dan)</label>
                    <br/>
                    <input type="date" name={"startDate"} onChange={onChangeDate} className={"myInput2"}
                           value={toDateInput(params?.startDate)}/>
                </div>
                <div className={"mx-4"}>
                    <label>Sana (gacha)</label>
                    <br/>
                    <input type="date" name={"endDate"} value={toDateInput(params?.endDate)}
                           className={"myInput2"}
                           min={toDateInput(params?.startDate)}
                           onChange={onChangeDate}/>
                </div>
                <div className={"mr-2"}>
                    <label>Turi</label>
                    <br/>
                    <select onChange={getOrderType} className={"myInput2"}>
                        <option value="">Buyurtma turi</option>
                        <option value={OrderType.TREASURY}>Oshxona</option>
                        <option value={OrderType.DELIVERY}>(Set) Yetqazish</option>
                    </select>
                </div>
                <div className={"flex items-center pt-2"}>
                    <MyDropDown item={deliveryState} list={deliveries} getItems={getDelivery} name={"Yetqazuvchi"}/>
                    <button onClick={() => getDelivery(undefined)}><MdClear size={30} color={'red'}/></button>
                </div>
            </nav>
            <div className={"mx-3"}>
                <Button variant={"outline-success"} onClick={onDownload} size={"sm"}>Excelga yuklash</Button>
                <Button onClick={() => handleShow()} variant={"outline-info"} className={"mx-2"} size={'sm'}>Taom
                    kesimida ko'rish</Button>

                <Table size={'sm'} bordered ref={tableRef} border={1}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Sana</th>
                        <th>Holati</th>
                        <th>Taom nomi</th>
                        <th>Taom soni</th>
                        <th>Taom narxi</th>
                        <th>Summasi</th>
                        <th>Umumiy narxi</th>
                    </tr>
                    </thead>
                    {orderList?.map((item, index) =>
                        <tbody key={index}>
                        <tr>
                            <td rowSpan={item.foodOrderDTOS?.length}>{index + 1}</td>
                            <td rowSpan={item.foodOrderDTOS?.length}>{new Date(item?.updateDate || 0).toLocaleString()}</td>
                            <td rowSpan={item.foodOrderDTOS?.length}
                                style={{color: colorSt(item.status)}}>{item.status}</td>
                            {
                                item?.foodOrderDTOS ?
                                    <>
                                        <td>{item?.foodOrderDTOS[0]?.foodDTO?.name}</td>
                                        <td>{item?.foodOrderDTOS[0]?.count}</td>
                                        <td>{item?.foodOrderDTOS[0]?.foodDTO?.price}</td>
                                        <td>{(item?.foodOrderDTOS[0]?.price ?? 1) * (item?.foodOrderDTOS[0]?.count ?? 1)}</td>
                                    </>
                                    : null

                            }
                            <td rowSpan={item.foodOrderDTOS?.length}>{item.foodOrderDTOS?.reduce((foodPrice, food) => {
                                return foodPrice + ((food?.price ?? 0)* (food?.count ?? 0))
                            }, 0)}</td>
                        </tr>
                        {
                            item?.foodOrderDTOS?.map((item2, index2) => {
                                    if (index2 !== 0) {
                                        return (
                                            <tr key={index2}>
                                                <td>{item2?.foodDTO?.name}</td>
                                                <td>{item2?.count}</td>
                                                <td>{item2?.foodDTO?.price}</td>
                                                <td>{(item2?.foodDTO?.price ?? 1) * (item2?.count ?? 1)}</td>
                                            </tr>)
                                    }
                                }
                            )
                        }
                        </tbody>
                    )}
                </Table>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <MyDropDown item={foodState} list={foods} getItems={getFoods} name={"Taomni tanlang"}/>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Soni</th>
                            <th>Narxi</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            foodList?.map((item, index) =>
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item?.foodDTO?.name}</td>
                                    <td>{item?.count}</td>
                                    <td>{item?.prices}</td>
                                </tr>
                            )
                        }
                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default OrderStatistics;