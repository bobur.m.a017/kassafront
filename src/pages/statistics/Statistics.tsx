import React, {useRef, useState} from 'react';
import {useChangeFoodOrderStatusMutation, useGetOrderQuery} from "../../reducers/order/OrderApi";
import {Button, OverlayTrigger, Table, Tooltip} from "react-bootstrap";
import {toDateInput} from "../../allFunc/AllFuncs";
import {useDownloadExcel} from "react-export-table-to-excel";
import {Colors} from "../../components/Strings";
import MyTabs from "../../components/MyTabs";
import OrderStatistics from "./OrderStatistics";
import EveryDay from "./EveryDay";
import DateByDate from "./DateByDate";

function Statistics() {
    const [tabNum, setTabNum] = useState<number>(0)


    return (
        <div>
            <div className={"px-4 bg-white mb-1 mr-1 myShadow myBorder myScroll p-3"}>
                <MyTabs lists={[{name: "Buyurtmalar", num: 0}, {name: "Umumiy", num: 1}, {name: "Kunlar", num: 2}]} onClickItem={setTabNum}/>
            </div>
            <div className={"flex justify-center w-100"}>
                {tabNum === 0 ?
                    <OrderStatistics/>
                    : null}
                {tabNum === 1 ? <DateByDate/> : null}
                {tabNum === 2 ? <EveryDay/> : null}
            </div>
        </div>

    );
}

export default Statistics;