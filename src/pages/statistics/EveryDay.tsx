import React, {useEffect, useState} from 'react';
import { useLazyGetWorkDaysQuery} from "../../reducers/workDay/WorkDayApi";
import {toDateInput, toDateView} from "../../allFunc/AllFuncs";
import {IOrder} from "../../DTO/OrderDTO";
import OneWorkDay from "./OneWorkDay";

interface IParams {
    startDate?: number,
    endDate?: number
}

function EveryDay() {
    const [tabNum, setTabNum] = useState(0)
    const [params, setParams] = useState<IParams>();
    const [orderState, setOrderState] = useState<IOrder | undefined>()

    const [getWorkDays, {data}] = useLazyGetWorkDaysQuery()
    useEffect(() => {
        console.log(data, "data")
    }, [data]);
    const onChangeDate = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        if (e) {
            setParams({...params, [e.target.name]: new Date(e?.target.value).getTime()});
            if (params?.endDate || e.target.name === "endDate") {
                getWorkDays({...params, [e.target.name]: new Date(e?.target.value).getTime()});
            }
        }
    }
    const openCompositions = (data: IOrder) => {
        if (data.id === orderState?.id) {
            setOrderState(undefined);
        } else {
            setOrderState(data);
        }
    }
    return (
        <div className={"max-w-[700px] bg-white w-100"}>
            <nav className={"h-24 flex justify-center py-3"}>
                <div>
                    <label>Sana (dan)</label>
                    <br/>
                    <input type="date" name={"startDate"} onChange={onChangeDate} className={"myInput2"}
                           value={toDateInput(params?.startDate)}/>
                </div>
                <div className={"mx-4"}>
                    <label>Sana (gacha)</label>
                    <br/>
                    <input type="date" name={"endDate"} value={toDateInput(params?.endDate)}
                           min={toDateInput(params?.startDate)}
                           className={"myInput2"}
                           disabled={!params?.startDate}
                           onChange={onChangeDate}/>
                </div>
            </nav>
            <div>
                {
                    data?.map((item, index) =>
                        <div key={index} className={"min-w-[400px]"}>
                            <div className={"myCard2 w-100"}>
                                <div className={"flex justify-between items-center myHover px-2"} onClick={() => openCompositions(item)}>
                                    <div>{toDateView(item?.createdDate).substring(0, 10)}</div>
                                    <span>Tushum: {item?.orderAllPrice}</span>
                                    <span>Kirim: {item?.inoutAllPrice}</span>
                                    <span>Chiqim: {item?.outputAllPrice}</span>
                                </div>
                                <div className={orderState?.id === item.id ? "animatedOpen close" : "animatedOpen"}>
                                   <OneWorkDay data={item}/>
                                </div>
                            </div>
                        </div>
                    )
                }

            </div>
        </div>
    );
}

export default EveryDay;