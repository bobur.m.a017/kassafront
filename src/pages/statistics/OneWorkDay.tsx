import React, {useEffect, useState} from 'react';
import {toDateInput, toDateView} from "../../allFunc/AllFuncs";
import {useLazyGetWorkDayQuery} from "../../reducers/workDay/WorkDayApi";
import {Splide, SplideSlide} from "@splidejs/react-splide";
import '@splidejs/react-splide/css/core';
import '@splidejs/react-splide/css';
import MyTabs from "../../components/MyTabs";
import ProductTables from "./ProductTables";
import {IWorkDay} from "../../DTO/WorkDayDTO";

interface IWorkDayProps {
    data?:IWorkDay
}

function DateByDate({data}:IWorkDayProps) {
    const [tabNum, setTabNum] = useState(0)

    return (
        <div className={"max-w-[700px] bg-white w-100"}>

            <Splide aria-label="My Favorite Images" options={{

                perPage: 1,
                padding: {left: 50, right: 30},
                mediaQuery: 'min',
                breakpoints: {
                    572: {
                        perPage: 2,
                    },
                }
            }}>
                <SplideSlide>
                    <div className={"myNavListStatistics"}>
                        <div>
                            <div className={"flex"}>
                                <span>Tushum: </span>
                                <span
                                    className={"text-green-800 font-bold mx-1 flex"}> {data?.orderAllPrice} so'm</span>
                            </div>
                            <div>
                                <span>Buyurtmalar soni: </span>
                                <span className={"text-green-800 font-bold mx-1"}> {data?.orderCount}</span>
                            </div>
                        </div>
                    </div>
                </SplideSlide>
                <SplideSlide>
                    <div className={"myNavListStatistics"}>
                        <div>
                            <div className={"flex"}>
                                <span>Kirim: </span>
                                <span
                                    className={"text-green-800 font-bold mx-1 flex"}> {data?.inoutAllPrice} so'm</span>
                            </div>
                            <div>
                                <span>Mahsulotlar soni: </span>
                                <span
                                    className={"text-green-800 font-bold mx-1"}> {data?.inoutProductDTOS?.length}</span>
                            </div>
                        </div>
                    </div>
                </SplideSlide>
                <SplideSlide>
                    <div className={"myNavListStatistics"}>
                        <div>
                            <div className={"flex"}>
                                <span>Chiqim: </span>
                                <span
                                    className={"text-green-800 font-bold mx-1 flex"}> {data?.outputAllPrice} so'm</span>
                            </div>
                            <div>
                                <span>Mahsulotlar soni: </span>
                                <span
                                    className={"text-green-800 font-bold mx-1"}> {data?.outputProductDTOS?.length}</span>
                            </div>
                        </div>
                    </div>
                </SplideSlide>
            </Splide>
            <div className={"flex justify-center py-2"}>
                <MyTabs lists={[{name: "kirim", num: 0}, {name: "chiqim", num: 1}, {name: "chiqim (real)", num: 2}, {name: "ombor", num: 3}]} onClickItem={setTabNum}/>
            </div>

                {tabNum === 0 ? <ProductTables list={data?.inoutProductDTOS} fileName={"kirim "+toDateView(data?.createdDate)?.substring(0,10)}/> : null}
                {tabNum === 1 ? <ProductTables list={data?.productCompositionSaveDTOS} fileName={"chiqim "+toDateView(data?.createdDate)?.substring(0,10)}/> : null}
                {tabNum === 2 ? <ProductTables list={data?.outputProductDTOS} fileName={"haqiqiy-chiqim "+toDateView(data?.createdDate)?.substring(0,10)}/> : null}
                {tabNum === 3 ? <ProductTables list={data?.dayWareHouseOfProductsDTOS} fileName={"ombor "+toDateView(data?.createdDate)?.substring(0,10)}/> : null}

        </div>
    );
}

export default DateByDate;