import React, {useEffect, useRef} from 'react';
import {useNavigate} from "react-router-dom";

function Default() {
    const update = useRef(false);
    const  history = useNavigate();
    useEffect(() => {
        if (!update.current){
            update.current = true
        }

    }, []);
    return (
        <div></div>
    );
}

export default Default;
