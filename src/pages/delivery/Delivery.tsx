import React, {useEffect, useState} from 'react';
import {
    useAddDeliveryMutation, useDeleteDeliveryMutation,
    useEditeDeliveryMutation,
    useGetDeliveryQuery
} from "../../reducers/delivery/DeliveryApi";
import {AiFillDelete, AiFillEdit} from "react-icons/ai";
import {IoMdAddCircle} from "react-icons/io";
import {Button, Form, Modal} from "react-bootstrap";
import {useGetUserRoleQuery} from "../../reducers/user/UserApi";
import {Roles} from "../../components/Strings";
import {IDelivery} from "../../DTO/DeliveryDTO";

function Categories() {
    const [delivery, setDelivery] = useState<IDelivery[] | undefined>()
    const [deliveryState, setDeliveryState] = useState<IDelivery | undefined>()
    const {data: deliveryDate, error: deliveryError, isLoading: loadingApi} = useGetDeliveryQuery('');
    const [addDeliveryApi, resultApi] = useAddDeliveryMutation();
    const [editDeliveryApi, resultEditeDeliveryApi] = useEditeDeliveryMutation();
    const [deleteDeliveryApi, resultDeleteDeliveryApi] = useDeleteDeliveryMutation();
    const [show, setShow] = useState(false);
    const {data: role} = useGetUserRoleQuery("");
    const handleClose = () => setShow(false);
    const handleShow = (data: undefined | IDelivery) => {
        setDeliveryState(data);
        setShow(true);
    };

    useEffect(() => {
        setDelivery(deliveryDate);
    }, [deliveryDate]);
    const submitData = (e: React.FormEvent<HTMLFormElement> | undefined) => {
        e?.preventDefault();
        if (deliveryState?.id) {
            editDeliveryApi(deliveryState);
        } else {
            addDeliveryApi(deliveryState);
        }
        handleClose();
    }
    const handlerChange = (e: React.ChangeEvent<HTMLInputElement> | undefined) => {
        if (e?.currentTarget?.name) {
            setDeliveryState({...deliveryState, [e?.currentTarget?.name]: e?.target.value})
        }
    }
    return (
        <div>
            <button className={"myBgButton myBorder flex justify-between"} onClick={() => handleShow(undefined)}>
                <IoMdAddCircle size={25} color={"#E5EDFC"}/> <span className={"flex mx-2"}>Yetqauvchi qo'shish</span>
            </button>
            <br/>
            <div className={"myScroll"}>
                {delivery?.map((item, index) =>
                    <div key={index} className={"w-100 flex justify-center"}>
                        <div className={"myList"}>
                            <div>{item.name}</div>
                            {role === Roles.ADMIN ? <div>
                                <button className={"mx-3"} onClick={() => handleShow(item)}><AiFillEdit
                                    color={"success"}/></button>
                                <button onClick={() => deleteDeliveryApi(item)}><AiFillDelete color={"red"}/></button>
                            </div> : null}
                        </div>
                    </div>
                )}
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id={"formControl"} onSubmit={submitData}>
                        <Form.Label>Yetqazuvchi nomi</Form.Label>
                        <input type="text" placeholder="kiriting" className={"myInput2"}
                               value={deliveryState?.name || ''}  name={"name"} onChange={handlerChange} required/>
                        <Form.Label>Yetqazuvchi tel raqami</Form.Label>
                        <input type="text" placeholder="kiriting" className={"myInput2"}
                               value={deliveryState?.phoneNumber || ''} name={"phoneNumber"} onChange={handlerChange} required/>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" type={'submit'} form={'formControl'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Categories;