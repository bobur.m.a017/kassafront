import React, { useState} from 'react';
import './login/style.scss'
import {loginUser} from "../allFunc/AllFuncs";
import {toast} from "react-toastify";
import LoginPage from "./login/LoginPage";

interface ILogin {
    children: JSX.Element
}
interface ILoginData {
    username?:string|undefined,
    password?:string|undefined
}
function UserLogin({children}: ILogin): JSX.Element {
    const [data,setData] = useState<ILoginData>()
    const changeData = (e:React.ChangeEvent<HTMLInputElement>) => {
        setData({...data,[e?.target?.name]:e?.target?.value})
    }
    const submit = () => {
        if(loginUser(data as ILoginData)){
            window.location.reload();
        }else {
            toast.error("Пользователь ненайдено ");
        }

    }
    const localStorageItem = localStorage.getItem("Authorization");
    if (localStorageItem?.startsWith("Bearer ")) {
        return children;
    } else {
        return (
            <div className={"w-100"}>
                <LoginPage/>
            </div>
        );
    }
}

export default UserLogin;
