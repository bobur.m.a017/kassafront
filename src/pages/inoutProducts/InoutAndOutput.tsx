import React, {useState} from 'react';
import MyTabs from "../../components/MyTabs";
import OutputProducts from "../outputProducts/OutputProduct";
import InoutProducts from "./InoutProduct";

function InoutAndOutput() {
    const [numTabs, setNumTabs] = useState(0)
    return (
        <div>
            <div className={"px-4 bg-white mb-1 mr-1 myShadow myBorder myScroll p-3"}>
                <MyTabs lists={[{name: 'Kirim', num: 0}, {name: 'Chiqim', num: 1}]} onClickItem={setNumTabs}/>
            </div>
            <div>
                {numTabs === 0 ? <InoutProducts/>:null}
                {numTabs === 1 ? <OutputProducts/>:null}
            </div>
        </div>
    );
}

export default InoutAndOutput;