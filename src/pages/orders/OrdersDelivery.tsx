import React, {useEffect, useState} from 'react';
import {ICategory} from "../../DTO/CategoryDTO";
import {useGetCategoryQuery} from "../../reducers/category/CategoryApi";
import {IFood} from "../../DTO/ProductDTO";
import imgs from "../../images/img.png";
import {
    useAddOrderDeliveryMutation,
    useDeleteFoodOrderMutation,
    useEditeOrderMutation
} from "../../reducers/order/OrderApi";
import {IFoodOrders, IOrder} from "../../DTO/OrderDTO";
import {BsFillCheckCircleFill} from "react-icons/bs";
import {Colors, OrderStatus} from "../../components/Strings";
import {GiCancel} from "react-icons/gi";
import {AiFillDelete, AiOutlineMinusCircle, AiOutlinePlusCircle} from "react-icons/ai";
import {useGetFoodsQuery} from "../../reducers/food/FoodApi";
import {toast} from "react-toastify";
import MyTabs from "../../components/MyTabs";
import OrderListPrint from "./OrderListPrint";

function OrderDelivery() {
    const [show, setShow] = useState(false);
    const [tabNumber, setTabNumber] = useState<number>(0)
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [orderState, setOrderState] = useState<IOrder | undefined>();
    const [productList, setProductList] = useState<IFood[] | undefined>()
    const [categoryState, setCategoryState] = useState<ICategory | undefined>()
    const {data: productDate, refetch: getFoods} = useGetFoodsQuery({categoryId: categoryState?.id});
    const {data: categoryData, error: categoryError, isLoading: loadingApi} = useGetCategoryQuery("");
    const [addOrderApi] = useAddOrderDeliveryMutation()
    const [editeOrderApi] = useEditeOrderMutation()
    const [deleteFoodOrder] = useDeleteFoodOrderMutation();

    useEffect(() => {
        setProductList([]);
        setProductList(productDate)
    }, [productDate]);
    const getProducts = (data:ICategory|undefined) => {
        setCategoryState(data);
        setProductList([]);
      if (data){
          setProductList(productDate?.filter((item)=>item?.category?.id === data.id))
      }else {
          setProductList(productDate);
      }
    }

    const creatOrder = (data: IFood | undefined) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            if (foodOrderDTOS.find((item => item?.foodDTO?.id === data?.id))) {
                orderState?.foodOrderDTOS.forEach((item, index) => {
                    if (item?.foodDTO?.id === data?.id) {
                        let food: IFoodOrders = {...foodOrderDTOS[index]};
                        // console.log((food?.prices || 0) + (data?.price || 0))
                        food = {
                            ...food,
                            prices: (food?.prices || 0) + (data?.price || 0),
                            count: (food?.count || 1) + 1
                        }
                        foodOrderDTOS[index] = {...food};
                    }
                })
            } else {
                let orderProduct: IFoodOrders = {count: 1, foodDTO: data, prices: data?.price};
                foodOrderDTOS.push(orderProduct);
            }
        } else {
            let orderProduct: IFoodOrders = {count: 1, foodDTO: data, prices: data?.price};
            foodOrderDTOS.push(orderProduct);
        }
        setOrderState({...orderState, foodOrderDTOS});
        // setOrderState({...orderState,foodOrderDTOS:})
    }
    const removeOrderGood = (data: IFoodOrders, index: number) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            if (data?.id) {
                deleteFoodOrder(data)
            }
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            foodOrderDTOS.splice(index, 1);
            setOrderState({...orderState, foodOrderDTOS});
        }
    }
    const removeOrderGoodCount = (data: IFood | undefined, index: number) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            if (foodOrderDTOS.find((item => item?.foodDTO?.id === data?.id))) {
                orderState?.foodOrderDTOS.forEach((item, index) => {
                    if (item?.foodDTO?.id === data?.id && (item.count || 1) > 1) {
                        let food: IFoodOrders = {...foodOrderDTOS[index]};
                        food = {
                            ...food,
                            prices: (food?.prices || 0) - (data?.price || 0),
                            count: (food?.count || 1) - 1
                        }
                        foodOrderDTOS[index] = {...food};
                    }
                })
            } else {
                foodOrderDTOS.splice(index, 1);
            }
        }
        setOrderState({...orderState, foodOrderDTOS});
    }
    const submitOrders = () => {
        let data = {
            ...orderState, prices: orderState?.foodOrderDTOS?.reduce((total, item) => {
                    return total + (item.prices || 0)
                }
                , 0)
        };
        if (data.id) {
            editeOrderApi(data);
        } else {
            addOrderApi(data);
        }
        setOrderState(undefined);
    }
    const getOrder = (data: IOrder) => {
        if (data.status === OrderStatus.PAID) {
            toast.error("Tolangan buyurtmani o'zgartiraolmaysiz")
        } else {
            setOrderState(data);
        }
        handleClose();
    }

    return (
        <div>
            <nav className={"flex py-2"}>
                <div className={"flex w-100 scrollStyle mx-2"} style={{overflowX: 'auto'}}>
                        <div className={"myNavList"} onClick={() => getProducts(undefined)}>
                            <div>Hammasi</div>
                            <div>
                                <input type="radio" checked={!categoryState?.id} readOnly/>
                            </div>
                        </div>
                    {categoryData?.map((item, index) =>
                            <div className={"myNavList"} onClick={() => getProducts(item)}>
                                <span>{item.name}</span>
                                <span>
                                    <input type="radio" checked={item.id === categoryState?.id} readOnly/>
                                </span>
                            </div>
                    )}
                </div>
            </nav>
            <div className={"w-100 h-100 md:flex h-[88vh]"}>
                <div className={"myScroll md:w-[30%] myBorderColor"}>
                    <div className={"flex justify-center"}>
                        <div className={"myListOrder"}>
                            <div className={"flex justify-between"}>
                                <div>Buyurtma</div>
                                <div className={"flex"}>
                                    <BsFillCheckCircleFill color={Colors.GOVERNESS_COLOR} size={30}
                                                           className={"mx-2 myHover"}
                                                           onClick={() => submitOrders()}/>
                                    <GiCancel size={30} color={"red"} onClick={() => setOrderState(undefined)}
                                              className={"myHover"}/>
                                </div>
                            </div>
                            <div>Umumiy narxi: <span className={"font-bold"}>
                                <div className={"flex justify-between w-100"}>
                                    <span className={"w-50"}>
                                        <label>Manzil</label>
                                        <input type="text" name={"address"} className={"myInput max-w-[100%]"} value={orderState?.address||""} onChange={(e)=>setOrderState({...orderState,address:e?.target?.value})} />
                                    </span>
                                    <span className={"w-50"}>
                                        <label>Tel:</label>
                                        <br/>
                                        <input type="text" className={"myInput max-w-[100%]"} value={orderState?.phoneNumber||""}  onChange={(e)=>setOrderState({...orderState,phoneNumber:e?.target?.value})} name={"phoneNumber"}/>
                                    </span>
                                </div>
                                {orderState?.foodOrderDTOS?.reduce((total, item) => {
                                        return total + (item.prices || 0)
                                    }
                                    , 0)}
                            </span>
                            </div>
                        </div>
                    </div>
                    <div className={"myCardOrder"}>
                        {
                            orderState?.foodOrderDTOS?.map((item, index) =>
                                <div className={"border-b-[1px] border-amber-950 p-2"} key={index}>
                                    <div className={"font-bold"}>
                                        {item?.foodDTO?.name?.substring(0, 24)}
                                    </div>
                                    <div className={"flex justify-between"}>
                                        <div>
                                            <span>narxi: </span>
                                            <span className={"font-bold"}>
                                        {item.prices}
                                        </span>
                                        </div>
                                        <div>
                                            <span>soni: </span>
                                            <span className={"font-bold"}>
                                        {item.count}
                                        </span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-between"}>
                                        <AiFillDelete color={"red"} className={"myHover"} size={20}
                                                      onClick={() => removeOrderGood(item, index)}/>
                                        <div className={"flex justify-between"}>
                                            <AiOutlinePlusCircle color={"green"} size={20} className={"mx-2 myHover"}
                                                                 onClick={() => creatOrder(item?.foodDTO)}/>
                                            <AiOutlineMinusCircle className={"myHover"} color={"red"} size={20}
                                                                  onClick={() => removeOrderGoodCount(item.foodDTO, index)}/>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <div
                    className={"w-[100%] h-100 md:w-[80%] lg:w-[75%] h-max max-h-[89.5vh]"}>
                    <div className={"px-4 myBorderColor2 myScroll p-3 w-100 bg-white"}>
                        <MyTabs lists={[{name: "Taomlar", num: 0}, {name: "Buyurtmalar", num: 1}]}
                                onClickItem={setTabNumber}/>
                    </div>
                    {tabNumber === 0 ? <div
                        className={"grid grid-cols-2 sm:grid-cols-3 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 myScroll scrollStyle w-100 place-items-center"}>
                        {
                            productList?.map((item, index) =>
                                <div key={index} className={"myCard mx-2"} style={{cursor: 'pointer'}}
                                     onClick={() => creatOrder(item)}>
                                    <div className={"w-100 h-[100px] imgCard"}
                                         style={{backgroundImage: `url(${item.imgUrl ? item.imgUrl : imgs})`}}>
                                    </div>
                                    <div className={"min-h-[50px]"}>
                                        <div className={"text-sm"}>
                                            {item.name?.substring(0,25)}
                                        </div>
                                        <div className={"font-bold"}>
                                            {item.price} so'm
                                        </div>
                                    </div>

                                </div>
                            )
                        }
                    </div> : null}
                    {tabNumber === 1 ? <div className={"w-100"}>
                        <OrderListPrint getOrder={getOrder}/>
                    </div> : null}
                </div>
            </div>
        </div>
    );
}

export default OrderDelivery;