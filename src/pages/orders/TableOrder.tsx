import React, {ReactInstance, useRef} from 'react';
import ReactToPrint from "react-to-print";
import {IFoodOrders, IOrder} from "../../DTO/OrderDTO";
import {Table} from "react-bootstrap";
import {AiFillPrinter} from "react-icons/ai";

interface IThis {
    item?: IOrder
}

function TableOrder({item}: IThis) {
    const componentRef = useRef(null);

    return (
        <div>
            <ReactToPrint
                trigger={() => <button className={"myButton myBorderColor2 myBorder flex p-1"}><span className={"mx-2"}>Print</span> <AiFillPrinter size={20}/></button>}
                content={() => componentRef?.current}
            />
            <div ref={componentRef} className={"p-3"}>

                <Table className={"myBorderColor2"} size={"sm"}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Soni</th>
                        <th>Narxi</th>
                        <th>Umumiy</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        item?.foodOrderDTOS?.map((foodOrder, key) =>
                            <tr key={foodOrder.id}>
                                <td>{key + 1}</td>
                                <td>{foodOrder?.foodDTO?.name}</td>
                                <td>{foodOrder?.count}</td>
                                <td>{foodOrder?.price}</td>
                                <td>{foodOrder?.prices}</td>
                            </tr>
                        )
                    }
                    <tr>
                        <td>-</td>
                        <td>Manzil</td>
                        <td>{item?.address}</td>
                        <td>Tel: {item?.phoneNumber}</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        </div>
    );
}

export default TableOrder;