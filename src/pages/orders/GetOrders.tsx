import React, {useState} from 'react';
import {useChangeFoodOrderStatusMutation, useGetOrderQuery} from "../../reducers/order/OrderApi";
import {IOrder} from "../../DTO/OrderDTO";
import {Colors, OrderStatus, OrderType} from "../../components/Strings";
import {BsPencilSquare} from "react-icons/bs";
import {toDateView} from "../../allFunc/AllFuncs";
import TableOrder from "./TableOrder";

const PINK = 'rgba(255, 192, 203, 0.6)';
const BLUE = 'rgba(0, 0, 255, 0.6)';

interface Interface {
    getOrder: (item: IOrder) => void,
}

function GetOrders({getOrder}: Interface) {
    const {data} = useGetOrderQuery({orderType: OrderType.TREASURY})
    const [changeStatus] = useChangeFoodOrderStatusMutation();
    const [orderState, setOrderState] = useState<IOrder | undefined>()

    const colorSt = (text: string | undefined): string => {
        if (text === OrderStatus.NO_PAID) {
            return Colors.NO_PAID
        } else {
            return Colors.PAID
        }
    }
    const openCompositions = (data: IOrder) => {
        if (data.id === orderState?.id) {
            setOrderState(undefined);
        } else {
            setOrderState(data);
        }
    }
    const totalSum = () => {
     let num = 0;
     data?.forEach((item,index)=>
     {

     })
    }
    const rerender = () => {
        return (
            <div>
                {
                    data?.map((item, index) =>
                        <div key={index} className={"min-w-[300px]"}>
                            <div className={"myCard2 w-100"}>
                                <div className={"flex justify-between items-center myHover"}>
                                    <div
                                        onClick={() => openCompositions(item)}>{toDateView(item.updateDate).substring(12, 17)}</div>
                                    <select name="status" style={{color: `${colorSt(item?.status)}`}}
                                            defaultValue={item.status} onChange={() => changeStatus(item)}
                                            disabled={item.status === OrderStatus.PAID}>
                                        <option value="To'lanmagan">To'lanmagan</option>
                                        <option value="To'langan">To'langan</option>
                                    </select>
                                    <span
                                        className={"font-semibold myColor"}>Narxi {item?.foodOrderDTOS?.reduce((total, item) => {
                                            return total + (item.prices || 0)
                                        }
                                        , 0)}</span>
                                    <BsPencilSquare color={"green"} size={25} onClick={() => getOrder(item)}
                                                    style={{cursor: 'pointer'}}/>
                                </div>
                                <div className={orderState?.id === item.id ? "animatedOpen close" : "animatedOpen"}>
                                    <TableOrder item={item}/>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }
    return (
        <div>
            <div>
                Umumiy qiymati.:
                <span className={"text-green-800 font-bold text-lg mx-1"}>
                {data?.reduce((num, item) => {
                        return num + (item?.foodOrderDTOS?.reduce((price, data2) => {
                            return price + ((data2?.prices || 0));
                        }, 0) || 0)
                    },
                    0)}
                </span>
            </div>
            {
                rerender()
            }

        </div>
    );
}

export default GetOrders;