import React, {useState} from 'react';
import {useChangeFoodOrderStatusMutation, useGetOrderQuery} from "../../reducers/order/OrderApi";
import {IOrder} from "../../DTO/OrderDTO";
import {Colors, OrderStatus, OrderType} from "../../components/Strings";
import {BsPencilSquare} from "react-icons/bs";
import TableOrder from "./TableOrder";
import {toDateView} from "../../allFunc/AllFuncs";
import {useGetDeliveryQuery, useRelationsDeliveryMutation} from "../../reducers/delivery/DeliveryApi";
import {IDelivery} from "../../DTO/DeliveryDTO";

interface Interface {
    getOrder: (item: IOrder) => void,
}

function GetOrders({getOrder}: Interface) {
    const {data} = useGetOrderQuery({orderType: OrderType.DELIVERY})
    const {data:deliveries} = useGetDeliveryQuery("");
    const [relationsDelivery] = useRelationsDeliveryMutation();
    const [changeStatus] = useChangeFoodOrderStatusMutation();
    const [orderState, setOrderState] = useState<IOrder | undefined>()
    const colorSt = (text: string | undefined): string => {
        if (text === OrderStatus.NO_PAID) {
            return Colors.NO_PAID
        } else {
            return Colors.PAID
        }
    }
    const openCompositions = (data: IOrder) => {
        if (data.id === orderState?.id) {
            setOrderState(undefined);
        } else {
            setOrderState(data);
        }
    }
    const relationDeliveryToOrder = (e: React.ChangeEvent<HTMLSelectElement>, data: IOrder) => {
        relationsDelivery({deliveryId: e.target.value, orderId: data.id})
        console.log({deliveryId: e.target.value, orderId: data.id})
    }
    return (
        <div className={"w-full pr-2 myScroll scrollStyle"}>
            <div>
                Umumiy qiymati:
                <span className={"text-green-800 font-bold text-lg mx-1"}>
                {data?.reduce((num, item) => {
                        return num + (item?.foodOrderDTOS?.reduce((price, data) => {
                            return price + ((data?.prices || 0));
                        }, 0) ?? 0)
                    },
                    0)}
                </span>
            </div>
            {
                data?.map((item, index) =>
                    <div key={index} className={"min-w-[400px]"}>
                        <div className={"myCard2 w-100"}>
                            <div className={"flex justify-between items-center myHover"}>
                                <div onClick={() => openCompositions(item)} >{toDateView(item.updateDate).substring(0,17)}</div>
                                <select name="status" style={{color: `${colorSt(item?.status)}`}}
                                        defaultValue={item.status} onChange={() => changeStatus(item)}
                                        disabled={item.status === "To'langan"}>
                                    <option value="To'lanmagan">To'lanmagan</option>
                                    <option value="To'langan">To'langan</option>
                                </select>
                                <span className={"font-semibold myColor"}>Narxi {item?.foodOrderDTOS?.reduce((total, item) => {
                                        return total + (item.prices || 0)
                                    }
                                    , 0)}</span>
                                <BsPencilSquare color={"green"} size={25} onClick={() => getOrder(item)}
                                                style={{cursor: 'pointer'}}/>
                            </div>
                            <div className={ orderState?.id === item.id ?"animatedOpen close":"animatedOpen"}>
                                <TableOrder item={item}/>
                                Yetqazuvchi: <select name="categoryId" defaultValue={item?.deliveryDTO?.id || ""}
                                                     onChange={(e) => relationDeliveryToOrder(e, item)}>
                                <option value="">Yetqazuvchi</option>
                                {
                                    deliveries?.map((delivery, index1) =>
                                        <option value={delivery.id}
                                                key={index1}>{delivery?.name?.substring(0, 10)}</option>
                                    )
                                }
                            </select>
                            </div>
                        </div>
                    </div>
                )
            }

        </div>
    );
}

export default GetOrders;