import React, {useEffect, useState} from 'react';
import {ICategory} from "../../DTO/CategoryDTO";
import {useGetCategoryQuery} from "../../reducers/category/CategoryApi";
import {IFood} from "../../DTO/ProductDTO";
import imgs from "../../images/img.png";
import {useAddOrderMutation, useDeleteFoodOrderMutation, useEditeOrderMutation} from "../../reducers/order/OrderApi";
import {IFoodOrders, IOrder} from "../../DTO/OrderDTO";
import {BsFillCheckCircleFill} from "react-icons/bs";
import {Colors, OrderStatus} from "../../components/Strings";
import {GiCancel} from "react-icons/gi";
import {AiFillDelete, AiOutlineMinusCircle, AiOutlinePlusCircle} from "react-icons/ai";
import {Button, Offcanvas} from "react-bootstrap";
import GetOrders from "./GetOrders";
import {useGetFoodsQuery} from "../../reducers/food/FoodApi";
import {toast} from "react-toastify";

function Orders() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [orderState, setOrderState] = useState<IOrder | undefined>();
    const [productList, setProductList] = useState<IFood[] | undefined>()
    const [productState, setProductState] = useState<IFood | undefined>()
    const [categoryState, setCategoryState] = useState<ICategory | undefined>()
    const {data: productDate, refetch: getFoods} = useGetFoodsQuery({categoryId: categoryState?.id});
    const {data: categoryData, error: categoryError, isLoading: loadingApi} = useGetCategoryQuery("");
    const [addOrderApi] = useAddOrderMutation()
    const [editeOrderApi] = useEditeOrderMutation()
    const [deleteFoodOrder] = useDeleteFoodOrderMutation();
    useEffect(() => {

        setProductList([]);
        setProductList(productDate)
    }, [productDate]);
    const creatOrder = (data: IFood | undefined) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            if (foodOrderDTOS.find((item => item?.foodDTO?.id === data?.id))) {
                orderState?.foodOrderDTOS.forEach((item, index) => {
                    if (item?.foodDTO?.id === data?.id) {
                        let food: IFoodOrders = {...foodOrderDTOS[index]};
                        // console.log((food?.prices || 0) + (data?.price || 0))
                        food = {
                            ...food,
                            prices: (food?.prices || 0) + (data?.price || 0),
                            count: (food?.count || 1) + 1
                        }
                        foodOrderDTOS[index] = {...food};
                    }
                })
            } else {
                let orderProduct: IFoodOrders = {count: 1, foodDTO: data, prices: data?.price};
                foodOrderDTOS.push(orderProduct);
            }
        } else {
            let orderProduct: IFoodOrders = {count: 1, foodDTO: data, prices: data?.price};
            foodOrderDTOS.push(orderProduct);
        }
        setOrderState({...orderState, foodOrderDTOS});
        // setOrderState({...orderState,foodOrderDTOS:})
    }
    const removeOrderGood = (data: IFoodOrders, index: number) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            if (data?.id) {
                deleteFoodOrder(data)
            }
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            foodOrderDTOS.splice(index, 1);
            setOrderState({...orderState, foodOrderDTOS});
        }
    }
    const removeOrderGoodCount = (data: IFood | undefined, index: number) => {
        let foodOrderDTOS: IFoodOrders[] = [];
        if (orderState?.foodOrderDTOS) {
            foodOrderDTOS = [...orderState?.foodOrderDTOS];
            if (foodOrderDTOS.find((item => item?.foodDTO?.id === data?.id))) {
                orderState?.foodOrderDTOS.forEach((item, index) => {
                    if (item?.foodDTO?.id === data?.id && (item.count || 1) > 1) {
                        let food: IFoodOrders = {...foodOrderDTOS[index]};
                        food = {
                            ...food,
                            prices: (food?.prices || 0) - (data?.price || 0),
                            count: (food?.count || 1) - 1
                        }
                        foodOrderDTOS[index] = {...food};
                    }
                })
            } else {
                foodOrderDTOS.splice(index, 1);
            }
        }
        setOrderState({...orderState, foodOrderDTOS});
    }
    const submitOrders = () => {
        let data = {
            ...orderState, prices: orderState?.foodOrderDTOS?.reduce((total, item) => {
                    return total + (item.prices || 0)
                }
                , 0)
        };
        if (data.id) {
            editeOrderApi(data);
        } else {
            addOrderApi(data);
        }
        setOrderState(undefined);
    }
    const getOrder = (data: IOrder) => {
        if (data.status === OrderStatus.PAID) {
            toast.error("Tolangan buyurtmani o'zgartiraolmaysiz")
        } else {
            setOrderState(data);
        }
        handleClose();
    }
    const getProducts = (data:ICategory|undefined) => {
        setCategoryState(data);
        setProductList([]);
        if (data){
            setProductList(productDate?.filter((item)=>item?.category?.id === data.id))
        }else {
            setProductList(productDate);
        }
    }

    return (
        <div>
            <nav className={"flex py-2"}>
                <div className={"flex w-100 scrollStyle mx-2"} style={{overflowX: 'auto'}}>
                    <div className={"flex justify-center mb-2"}>
                        <Button variant="primary" onClick={handleShow}>
                            Buyurtmalar
                        </Button>
                        <div className={"myNavList"} onClick={() => getProducts(undefined)}>
                            <div>Hammasi</div>
                            <div>
                                <input type="radio" checked={!categoryState?.id} readOnly/>
                            </div>
                        </div>
                    </div>
                    {categoryData?.map((item, index) =>
                        <div key={index} className={"flex justify-center"}>
                            <div className={"myNavList"} onClick={() => getProducts(item)}>
                                <div>{item.name}</div>
                                <div>
                                    <input type="radio" checked={item.id === categoryState?.id} readOnly/>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </nav>
            <div className={"w-100 h-100 md:flex h-[88vh]"}>
                <div className={"myScroll md:w-[30%] myBorderColor"}>
                    <div className={"flex justify-center"}>
                        <div className={"myListOrder"}>
                            <div className={"flex justify-between"}>
                                <div>Buyurtma</div>
                                <div className={"flex"}>
                                    <BsFillCheckCircleFill color={Colors.GOVERNESS_COLOR} size={30}
                                                           className={"mx-2 myHover"}
                                                           onClick={() => submitOrders()}/>
                                    <GiCancel size={30} color={"red"} onClick={() => setOrderState(undefined)}
                                              className={"myHover"}/>
                                </div>
                            </div>
                            <div>Umumiy narxi: <span className={"font-bold"}>
                                {orderState?.foodOrderDTOS?.reduce((total, item) => {
                                        return total + (item.prices || 0)
                                    }
                                    , 0)}
                            </span>
                            </div>
                        </div>
                    </div>
                    <div className={"myCardOrder"}>
                        {
                            orderState?.foodOrderDTOS?.map((item, index) =>
                                <div className={"border-b-[1px] border-amber-950 p-2"} key={index}>
                                    <div className={"font-bold"}>
                                        {item?.foodDTO?.name?.substring(0, 24)}
                                    </div>
                                    <div className={"flex justify-between"}>
                                        <div>
                                            <span>narxi: </span>
                                            <span className={"font-bold"}>
                                        {item.prices}
                                        </span>
                                        </div>
                                        <div>
                                            <span>soni: </span>
                                            <span className={"font-bold"}>
                                        {item.count}
                                        </span>
                                        </div>
                                    </div>
                                    <div className={"flex justify-between"}>
                                        <AiFillDelete color={"red"} className={"myHover"} size={20}
                                                      onClick={() => removeOrderGood(item, index)}/>
                                        <div className={"flex justify-between"}>
                                            <AiOutlinePlusCircle color={"green"} size={20} className={"mx-2 myHover"}
                                                                 onClick={() => creatOrder(item?.foodDTO)}/>
                                            <AiOutlineMinusCircle className={"myHover"} color={"red"} size={20}
                                                                  onClick={() => removeOrderGoodCount(item.foodDTO, index)}/>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className={"w-[100%] h-100 md:w-[80%] lg:w-[75%] flex justify-center h-max max-h-[89.5vh]"}>
                    <div
                        className={"grid grid-cols-2 sm:grid-cols-3 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 myScroll scrollStyle w-100 place-items-center"}>
                        {
                            productList?.map((item, index) =>
                                <div key={index} className={"myCard mx-2"} style={{cursor: 'pointer'}}
                                     onClick={() => creatOrder(item)}>
                                    <div className={"w-100 h-[100px] imgCard"}
                                         style={{backgroundImage: `url(${item.imgUrl ? item.imgUrl : imgs})`}}>
                                    </div>
                                    <div className={"min-h-[50px]"}>
                                        <div className={"text-sm"}>
                                            {item.name?.substring(0,25)}
                                        </div>
                                        <div className={"font-bold"}>
                                            {item.price} so'm
                                        </div>
                                    </div>

                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
            <Offcanvas show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>Buyurtmalar </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <GetOrders getOrder={getOrder}/>
                </Offcanvas.Body>
            </Offcanvas>
        </div>
    );
}

export default Orders;