import React, {useEffect, useState} from 'react';
import "./styles/index2.scss"
import { Route, Routes, useNavigate} from "react-router-dom";
import 'swiper/swiper-bundle.min.css';
import 'swiper/css';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import UserLogin from "./pages/UserLogin";
import Admin from "./pages/admin/Admin";

function App() {
    const history = useNavigate();
    const [params, setParams] = useState("");
    useEffect(() => {
        // booksSearch.refetch();
    }, [params]);

    return (
        <div style={{backgroundColor:'#F3F4F4',width:'100%'}}>
                <Routes>
                    <Route path={"/*"} element={
                        <UserLogin>
                            <Admin/>
                        </UserLogin>
                    }/>
                </Routes>
            <ToastContainer/>
        </div>
    );
}

export default App;
