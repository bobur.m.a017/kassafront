import React from 'react';
import logo from "../images/logo.png";
import {Link} from "react-router-dom";
import { BiExit} from "react-icons/bi";
import {useAppSelector} from "../hooks/Hooks";
import {getLinks} from "./MyLinks";

function DesktopNavbar() {
    const {role} = useAppSelector(state => state.loading);
    const exit = () => {
        // window.location.replace("/");
        localStorage.removeItem("Authorization");
        window.history.pushState("obj", "", "/");
        window.location.reload();
    }
    return (
        <div className={"my-navbar shadow-2xl w-full hidden md:block"}>
            <div className={"position-absolute mt-1 w-full h-100"}>
                <div className={"flex items-center w-[100px] mb-3 mx-2"}>
                    <img src={logo} alt="" width={35}/>
                    <span className={"mx-3"}>Business</span>
                </div>
                {getLinks(role)?.map((item,index)=>
                <Link to={item.toLink} className={"no-link"} key={index}>
                    <div className={"item-nav flex items-center w-[100px]"}>
                        {item.icon}
                        <span className={"mx-3"}>{item.name}</span>
                    </div>
                </Link>
                )}
                <div className={"flex items-center item-nav-exit myHover"} onClick={() => exit()}>
                    <BiExit size={25}/>
                    <span className={"mx-3"}>Chiqish</span>
                </div>
            </div>
        </div>
    );
}

export default DesktopNavbar;