import {Link} from "react-router-dom";
import {BiCategory} from "react-icons/bi";
import {IoFastFoodOutline} from "react-icons/io5";
import {AiOutlineFileDone} from "react-icons/ai";
import {IoMdStats} from "react-icons/io";
import React from "react";
import {Roles} from "./Strings";
import {BsCartPlus} from "react-icons/bs";
import {FaTruck, FaUserCog, FaUsersCog, FaWarehouse} from "react-icons/fa";
import {FiSettings} from "react-icons/fi";


const user = () => {
    return (
        [
            {
                name: "Buyurtma",
                icon: <AiOutlineFileDone size={25}/>,
                toLink: "/orders"
            },
            {
                name: "Statistika",
                icon: <IoMdStats size={25}/>,
                toLink: "/statistic"
            },
            {
                name: "Ombor",
                icon: <FaWarehouse size={25}/>,
                toLink: "/warehouse-product"
            },
        ]
    )
}

const admin = () => {
    return (
        [
            {
                name: "Taom turlari",
                icon: <BiCategory size={25}/>,
                toLink: "/categories"
            },
            {
                name: "Taomlar",
                icon: <IoFastFoodOutline size={25}/>,
                toLink: "/products"
            },
            {
                name: "Sozlamalar",
                icon: <FiSettings size={25}/>,
                toLink: "/settings"
            },
            {
                name: "Kirim-Chiqim",
                icon: <BsCartPlus size={25}/>,
                toLink: "/inout-output"
            },
            {
                name: "Statistika",
                icon: <IoMdStats size={25}/>,
                toLink: "/statistic"
            },
            {
                name: "Ombor",
                icon: <FaWarehouse size={25}/>,
                toLink: "/warehouse-product"
            },
        ]
    )
}
const superAdmin = () => {
    return (
        [
            {
                name: "Statistika",
                icon: <IoMdStats size={25}/>,
                toLink: "/statistic-super"
            },
        ]
    )
}
const director = () => {
    return (
        [
            {
                name: "Statistika",
                icon: <IoMdStats size={25}/>,
                toLink: "/statistic"
            },
            {
                name: "Sozlamalar",
                icon: <FiSettings size={25}/>,
                toLink: "/settings-admin"
            },
            {
                name: "Xodimlar",
                icon: <FaUsersCog size={25}/>,
                toLink: "/users"
            },
            {
                name: "Ombor",
                icon: <FaWarehouse size={25}/>,
                toLink: "/warehouse-product"
            },
        ]
    )
}
const delivery = () => {
    return (
        [
            {
                name: "Buyurtma",
                icon: <FaTruck size={25}/>,
                toLink: "/orders-delivery"
            },
            {
                name: "Statistika",
                icon: <IoMdStats size={25}/>,
                toLink: "/statistic"
            },
            {
                name: "Ombor",
                icon: <FaWarehouse size={25}/>,
                toLink: "/warehouse-product"
            },
        ]
    )
}
export const getLinks = (roleName: string | undefined) => {
    if (Roles.USER === roleName) {
        return user();
    } else if (Roles.ADMIN === roleName) {
        return admin();
    } else if (Roles.SUPER_ADMIN === roleName) {
        return superAdmin();
    } else if (Roles.DIRECTOR === roleName) {
        return director();
    } else if (Roles.DELIVERY === roleName) {
        return delivery();
    }
}
