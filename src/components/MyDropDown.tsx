import React, {useState} from 'react';
import {Dropdown, Form} from "react-bootstrap";

const CustomToggle = React.forwardRef(({
                                           children,
                                           onClick
                                       }: any, ref: React.LegacyRef<HTMLButtonElement> | undefined) => (
    <button
        className={"myButtonInfo"}
        type={'button'}
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
    </button>
));

// forwardRef again here!
// Dropdown needs access to the DOM of the Menu to measure it
const CustomMenu = React.forwardRef(
    ({
         children,
         style,
         className,
         'aria-labelledby': labeledBy
     }: any | undefined, ref: React.LegacyRef<HTMLDivElement> | undefined) => {
        const [value, setValue] = useState('');
        return (
            <div
                ref={ref}
                style={style}
                className={className}
                aria-labelledby={labeledBy}
            >
                <Form.Control
                    autoFocus
                    className="mx-3 my-2 w-auto"
                    placeholder="Type to filter..."
                    onChange={(e) => setValue(e.target.value)}
                    value={value}
                />
                <ul className="list-unstyled">
                    {React.Children.toArray(children).filter(
                        (child) =>
                            // @ts-ignore
                            !value || child?.props?.children?.toLowerCase().includes(value),
                    )}
                </ul>
            </div>
        );
    },
);

interface IMyInterface {
    item: any,
    list: any[]|undefined
    getItems:(any:any)=>void|undefined,
    name?:string|undefined
}

function MyDropDown({list, item,getItems,name}: IMyInterface) {
    return (
        <div>
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                    {item?.name?? name ?? "Mahsulotni tanlang"}
                </Dropdown.Toggle>

                <Dropdown.Menu as={CustomMenu}>
                    {
                        list?.map((thisItem, index) =>
                            <Dropdown.Item key={index} onClick={()=>getItems(thisItem)}>{thisItem?.name}</Dropdown.Item>
                        )
                    }
                </Dropdown.Menu>
            </Dropdown>
        </div>
    );
}

export default MyDropDown;