import React, {useState} from 'react';
interface list {
    name:string,
    num:number
}
interface tabs {
    lists:list[]
    onClickItem:(num:number)=>void
}
function MyTabs({lists,onClickItem}:tabs) {
    const [curren, setCurren] = useState(0);
    const clickNum = (num:number) => {
      setCurren(num);
      onClickItem(num);
    }
    return (
        <div>
            {lists.map((num,index)=>
                <button key={index} onClick={()=>clickNum(num.num)} className={`${num.num===curren?"activeTabs":"inActiveTabs"} btn`}>{num.name}</button>
            )}
        </div>
    );
}

export default MyTabs;
