export enum Colors {
    GOVERNESS_COLOR = "#00A859",
    PAID = "#00A859",
    NO_PAID = "#a81900"
}

export enum Roles {
    ADMIN = "ROLE_ADMIN",
    USER = "ROLE_USER",
    SUPER_ADMIN = "ROLE_SUPER_ADMIN",
    DELIVERY = "ROLE_DELIVERY_ADMIN",
    DIRECTOR = "ROLE_DIRECTOR",
}

export enum OrderStatus {
    PAID = "To'langan",
    NO_PAID = "To'lanmagan",
}

export enum OrderType {
    TREASURY = "G'azna",
    DELIVERY = "Yetkazish"
}