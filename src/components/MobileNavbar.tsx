import React from 'react';
import logo from "../images/logo.png";
import {Link} from "react-router-dom";
import {BiCategory} from "react-icons/bi";
import {IoFastFoodOutline} from "react-icons/io5";
import {AiOutlineFileDone} from "react-icons/ai";
import {IoMdStats} from "react-icons/io";
import {Roles} from "./Strings";
import {useAppSelector} from "../hooks/Hooks";
import {getLinks} from "./MyLinks";

function DesktopNavbar() {
    const {role} = useAppSelector(state => state.loading);
    const exit = () => {
        // window.location.replace("/");
        localStorage.removeItem("Authorization");
        window.history.pushState("obj", "", "/");
        window.location.reload();
    }
    return (
        <div className={"my-navbar-mobile shadow-2xl w-full md:hidden"}>
            <div className={"position-absolute w-full flex justify-around px-2 p-1"}>
                <div className={"items-center nav-item-mobile"} onClick={() => exit()}>
                    <div className={"flex justify-center"}>
                        <img src={logo} alt="" width={25}/>
                    </div>
                    <span className={""}>Business</span>
                </div>
                {getLinks(role)?.map((item, index) =>

                    <Link to={item.toLink} className={"no-link"} key={index}>
                        <div className={"nav-item-mobile"}>
                            <div className={"flex justify-center"}>
                                {item.icon}
                            </div>
                            <span className={""}>{item.name}</span>
                        </div>
                    </Link>
                )
                }

            </div>
        </div>
    );
}

export default DesktopNavbar;